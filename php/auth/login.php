<?php
session_start();

include_once("../conn/index.php");
include_once('anti_injection.php');

$login = anti_injection($_REQUEST['login']);
$password = anti_injection($_REQUEST['password']);

$sql = "SELECT * FROM employees WHERE login = '$login' AND password = '$password' AND status = 1";
$res = mysqli_query($conn, $sql);

$array_permissions = array();
while ($row = mysqli_fetch_array($res)) {
	$_SESSION['ZWxldHJpY2Ft'] = $row['id'];
	$employee_id = $row['id'];

	$sql = "SELECT permission_id FROM employees_permissions WHERE employee_id = $employee_id";
	$res_permissions = mysqli_query($conn, $sql);
	while ($row_p = mysqli_fetch_array($res_permissions)) {
		array_push($array_permissions, $row_p['permission_id']);
		$permissao = $row_p[0][0];
	}

	if ($permissao == 1) {
		$tela = "agenda";
	} else if ($permissao == 2) {
		$tela = "orcamento";
	} else if ($permissao == 3) {
		$tela = "cliente";
	} else if ($permissao == 4) {
		$tela = "fornecedor";
	} else if ($permissao == 5) {
		$tela = "funcionario";
	} else if ($permissao == 6) {
		$tela = "automoveis";
	} else if ($permissao == 7) {
		$tela = "produtos";
	} else if ($permissao == 8) {
		$tela = "equipamentos";
	} else if ($permissao == 9) {
		$tela = "servico";
	} else if ($permissao == 10) {
		$tela = "contas-pagar";
	} else if ($permissao == 11) {
		$tela = "contas-receber";
	}else{
		$tela = "default";
	}

	$_SESSION['tela_inicial'] = $tela;

	$_SESSION['permissions'] = $array_permissions;

	exit(header('Location: ../../index.php'));
}

exit(header('Location: ../../login.php'));
