<?php
include_once("../../conn/index.php");

$id = $_GET['id'];

$sql = "SELECT * FROM services WHERE id = $id";
$res = mysqli_query($conn, $sql);

$data = array();
while ($row = mysqli_fetch_array($res)) {
    array_push($data, array(
        'name' => $row['name'],
        'hours_amount' => $row['hours_amount'],
        'hours_cost' => $row['hours_cost'],
        'infrastructure_cost' => $row['infrastructure_cost'],
        'sale_value' => $row['sale_value'],
        'profit_value' => $row['profit_value'],
    ));
}

mysqli_close($conn);
$json = json_encode($data);
echo $json;

?>