<?php
include_once("../../conn/index.php"); 

$sql = "SELECT * FROM enterprise";
$res = mysqli_query($conn, $sql);

$data  = array();
while ($row = mysqli_fetch_array($res)) {
    array_push($data,array('fantasy_name' => $row['fantasy_name']));
    array_push($data,array('corporate_name' => $row['corporate_name']));
    array_push($data,array('document' => $row['document']));
    array_push($data,array('email' => $row['email']));
    array_push($data,array('zip' => $row['zip']));
    array_push($data,array('street' => $row['street']));
    array_push($data,array('number' => $row['number']));
    array_push($data,array('neighborhood' => $row['neighborhood']));
    array_push($data,array('city' => $row['city']));
    array_push($data,array('state' => $row['state']));
    array_push($data,array('complement' => $row['complement']));
    array_push($data,array('phone' => $row['phone']));
}

mysqli_close($conn);
$json = json_encode($data);
echo $json;

?>