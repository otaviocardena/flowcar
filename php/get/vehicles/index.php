<?php
include_once("../../conn/index.php");

$id = $_GET['id'];

$sql = "SELECT * FROM vehicles WHERE id = $id";
$res = mysqli_query($conn, $sql);

$data = array();
while ($row = mysqli_fetch_array($res)) {
    array_push($data, array(
        'client_id' => $row['client_id'], 
        'model' => $row['model'],
        'brand' => $row['brand'],
        'color' => $row['color'],
        'car_year' => $row['car_year'],
        'model_year' => $row['model_year'],
        'license_plate' => $row['license_plate'],
        'photo_name' => $row['vehicle_photo_name'],
    ));
}

mysqli_close($conn);
$json = json_encode($data);
echo $json;
