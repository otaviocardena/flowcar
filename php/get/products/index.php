<?php
include_once("../../conn/index.php");

$id = $_GET['id'];

$sql = "SELECT * FROM products WHERE id = $id";
$res = mysqli_query($conn, $sql);

$data = array();
while ($row = mysqli_fetch_array($res)) {
    array_push($data, array(
        'name' => $row['name'],
        'description' => $row['description'],
        'cost_value' => $row['cost_value'],
        'measurement' => $row['measurement'],
        'quantity' => $row['quantity'],
    ));
}

mysqli_close($conn);
$json = json_encode($data);
echo $json;
