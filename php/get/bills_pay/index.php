<?php
include_once("../../conn/index.php");

$id = $_GET['id'];

$sql = "SELECT b.provider_id, b.title, b.description, b.bills_type, b.payment_date, b.create_date, b.payment_value, p.fantasy_name FROM bills_pay AS b LEFT JOIN providers AS p ON b.provider_id = p.id WHERE b.id = $id";
$res = mysqli_query($conn, $sql);

$data = array();
while ($row = mysqli_fetch_array($res)) {
    array_push($data, array(
        'provider_id' => $row['provider_id'], 
        'title' => $row['title'],
        'description' => $row['description'],
        'bills_type' => $row['bills_type'],
        'payment_date' => date('Y-m-d', strtotime($row['payment_date'])),
        'create_date' => date('Y-m-d', strtotime($row['create_date'])),
        'payment_value' => $row['payment_value'],
        'fantasy_name' => $row['fantasy_name'],
    ));
}

mysqli_close($conn);
$json = json_encode($data);
echo $json;
