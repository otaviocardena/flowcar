<?php
include_once("../../conn/index.php");

$employee_id = $_GET['id'];

$sql = "SELECT * FROM employees WHERE id = $employee_id";
$res = mysqli_query($conn, $sql);

$sql = "SELECT * FROM employees_permissions WHERE employee_id = $employee_id";
$res_permissions = mysqli_query($conn, $sql);
$count_permisssions = mysqli_num_rows($res_permissions);

$data = array();
while ($row = mysqli_fetch_array($res)) {
	$cont = 0;
	$permissions_ids = "";
	while ($row_p = mysqli_fetch_array($res_permissions)) {
		$cont += 1;
		// $permissions_ids .= "'";
		$permissions_ids .= $row_p['permission_id'];
		// $permissions_ids .= "'";

		if ($cont != $count_permisssions) {
			$permissions_ids .= ",";
		}
	}
	array_push($data, array(
		'login' => $row['login'],
		'password' => $row['password'],
		'name' => $row['name'],
		'document' => $row['document'],
		'salary_amount' => $row['salary_amount'],
		'role' => $row['role'],
		'zip' => $row['zip'],
		'street' => $row['street'],
		'number' => $row['number'],
		'neighborhood' => $row['neighborhood'],
		'city' => $row['city'],
		'state' => $row['state'],
		'complement' => $row['complement'],
		'permissions' => $permissions_ids
	));
}

mysqli_close($conn);
$json = json_encode($data);
echo $json;
