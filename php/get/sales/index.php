<?php
include_once("../../conn/index.php");

$id = $_GET['id'];

$sql = "SELECT * FROM sales WHERE id = $id";
$res = mysqli_query($conn, $sql);

$data = array();
while ($row = mysqli_fetch_array($res)) {
    array_push($data, array(
        'client_id' => $row['client_id'],
        'responsible_name' => $row['responsible_name'],
        'document_type' => $row['document_type'],
        'document' => $row['document'],
        'license_plate' => $row['license_plate'],
        'payment_date' => date('d/m/Y', strtotime($row['payment_date'])),
        'sale_obs' => $row['sale_obs'],
        'discount' => $row['discount'],
        'total_value' => $row['total_value'],
        'schedule_date' => date('Y-m-d', strtotime($row['schedule_date'])),
        'schedule_time' => $row['schedule_time'],
        'car_milage' => $row['car_milage'],
        'car_fuel' => $row['car_fuel'],
        'driver_obs' => $row['driver_obs'],
        'passenger_obs' => $row['passenger_obs'],
        'front_obs' => $row['front_obs'],
        'back_obs' => $row['back_obs'],
        'ceiling_obs' => $row['ceiling_obs'],
        'img_1' => $row['img_1'],
        'img_2' => $row['img_2'],
        'img_3' => $row['img_3'],
        'img_4' => $row['img_4'],
    ));
}

mysqli_close($conn);
$json = json_encode($data);
echo $json;

?>