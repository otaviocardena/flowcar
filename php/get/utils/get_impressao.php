<?php
include_once('../../conn/index.php');

$id_sales = $_GET['id'];

$sql = "SELECT * FROM sales WHERE id = $id_sales";
$res_sales = mysqli_query($conn, $sql);

while ($row = mysqli_fetch_array($res_sales)) {

    if ($row['status'] == 0) {
        $status = "Orçamento Aberto";
    } else if ($row['status'] == 1) {
        $status = "Agendado";
    } else {
        $status = "Cancelado";
    }

    $client_id = $row['client_id'];
    $responsible_name = $row['responsible_name'];
    $document_type = $row['document_type'];
    $document = $row['document'];
    $license_plate = $row['license_plate'];
    $payment_date = $row['payment_date'];
    $sale_obs = $row['sale_obs'];
    $discount = $row['discount'];
    $total_value = $row['total_value'];
    $schedule_date = $row['schedule_date'];
    $schedule_time = $row['schedule_time'];
    $car_milage = $row['car_milage'];
    $car_fuel = $row['car_fuel'];
    $driver_obs = $row['driver_obs'];
    $passenger_obs = $row['passenger_obs'];
    $front_obs = $row['front_obs'];
    $back_obs = $row['back_obs'];
    $ceiling_obs = $row['ceiling_obs'];
    $img_1 = $row['img_1'];
    $img_2 = $row['img_2'];
    $img_3 = $row['img_3'];
    $img_4 = $row['img_4'];
}

$cpf_show = '';
$cnpj_show = '';
$document_cnpj = '';
$document_cpf = '';

if ($document_type == 'cpf') {
    $cpf_show = 'show';
    $cnpj_show = 'hide';
    $document_cpf = $document;
    $document_cnpj = "";
} else {
    $cpf_show = 'hide';
    $cnpj_show = 'show';
    $document_cnpj = $document;
    $document_cpf = "";
}

$cont = 1;

$sql = "SELECT 
            ss.service_id,
            s.sale_value,
            s.name,
            s.description
        FROM sales_services AS ss
        INNER JOIN services AS s ON
            s.id = ss.service_id
        WHERE sale_id = $id_sales";
$res_sales_services = mysqli_query($conn, $sql);
$qtd_services_edit = mysqli_num_rows($res_sales_services);

$servicos = "";

?>



<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="../../../img/favicon.png" rel="icon">
    <link href="../../../img/favicon.png" rel="apple-touch-icon">

    <title>Flow Car</title>

    <!-- Custom fonts for this template-->
    <link href="../../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.1/css/selectize.default.css" rel="stylesheet" type="text/css">

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">

    <!-- Custom styles for this template-->
    <link href="../../../css/style.css" rel="stylesheet">
    <style>
        * {
            font-weight: bold;
            font-size: 1.1rem;
        }

        label {
            display: block;
            margin-bottom: 0rem;
            font-size: 1.2rem;
        }

        .black-color {
            color: #161616;
        }

        body {
            background-color: white;
        }
    </style>
</head>

<body>
    <div class="header">
        <div class="row">
            <div class="col text-left">
                <img src="../../../img/flowlet.png" style="width:65%" /></br>
                <!-- <img src="../../img/lerrilogo.png" style="width: 25%;text-align: right; margin-left: 265px;margin-top: -26px;" /> -->
            </div>

            <div class="col text-right">

                <h4><b>Orçamento Nº <span><?= $id_sales ?></span></b></h4>
                <label>Sitio Lerri, S/N, Bairro do Rincão, Itapetininga-SP</label>
                <label>Razão Social: Allan Timoteo Oliveira Lerri</labe>
                    <label>22.163.798/0001-25</label>
                    <label>Razão Social: Maria Nazaret Duarte Lerri</labe>
                        <label>27.588.162/0001-49</label>
                        <label>contatolerrigramas@gmail.com</label>
            </div>
        </div>
    </div>
    <br><br>
    <hr>
    <br><br>
    <div class="descricao">

        <div class="row">
            <div class="col-md-6">
                <label><b>Data Pagamento: </b><span class="black-color"><?= date('d/m/Y', strtotime($payment_date)) ?></span></label><br>

            </div>
            <div class="col-md-6">
                <label><b>Situação do Orçamento: </b><span class="black-color"><?= $status; ?></span></label><br>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label><b>Nome: </b><span class="black-color"><?= $responsible_name; ?></span></label>
                <label><b>Documento: </b><span class="black-color"><?= $document ?></span></label>
            </div>
        </div>
    </div>
    <br><br>
    <hr>
    <br><br>
    <div id="servicos_produtos">
        <div class="row">
            <div class="col-md-6">
                <label><b>Serviços</b></label>
            </div>
            <div class="col-md-2">
                <label><b>Descrição</b></label>
            </div>
            <div class="col-md-2">
                <label><b>Valor</b></label>
            </div>
        </div>
        <?php while ($row = mysqli_fetch_array($res_sales_services)) {
        ?>

            <div class="row">
                <div class="col-md-6">
                    <label class="black-color"><?= $row['name']; ?></label>
                </div>
                <div class="col-md-2">
                    <label class="black-color"><?= $row['description'] ?></label>
                </div>
                <div class="col-md-2">
                    <label class="black-color">R$ <?php echo number_format($row['sale_value'], 2, '.', ''); ?></label>
                </div>
            </div>
        <?php } ?>
        <br>
        <br>
        <!-- <div class="row">
            <div class="col-md-6">
                <label><b>Desconto</b></label>
            </div>
            <div class="col-md-2">

            </div>
            <div class="col-md-2">
                <label><b>Valor Total</b></label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label class="black-color"><?= number_format($discount, 2, '.', ''); ?></label>
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-2">
                <label class="black-color">R$ <?php echo number_format($desconto, 2, '.', ''); ?></label>
            </div>
        </div> -->

        <br>
        <div class="row">
            <div class="col-6">
                <label><b></b></label>
            </div>
            <div class="col-2">
                <label><b>Quantidade Serviços</b></label>
            </div>
            <div class="col-2">
                <label><b>Desconto</b></label>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <label><b></b></label>
            </div>
            <div class="col-2">
                <label class="black-color"><b><?= $qtd_services_edit ?></b></label>
            </div>
            <div class="col-2">
                <label class="black-color"><b>R$<?= number_format($discount, 2, '.', ''); ?></b></label>
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-6">
                <label><b>Total Geral</b></label>
            </div>
            <div class="col-2">
                <label><b></b></label>
            </div>
            <div class="col-2">
                <label><b>R$ <?php echo number_format($total_value, 2, '.', ''); ?></b></label>
            </div>
        </div>
    </div>
    <br>
    <!-- <label class="divider"></label>
    <br><br> -->
    <!-- <div class="assinatura">
        <label>___________________________________</label>
        <label><b>Fernando Aparecido Camargo</b></label>
        <label>Engenheiro responsável</label>
        <label>CREA: 5062237383</label>
    </div> -->
    <div style="text-align:center; margin-top: 5px;border-top: 1px solid #000; background:#fff;">
        <h3 style="margin-top: 5px;">INFORMATIVO</h3>
        <p>Entrega: 2 a 3 dias úteis, após confirmação do pedido, salvo chuvas. (Avisaremos 1 dia antes da entrega)
            </br>Mão de obra da descarga: por conta do comprador (Salvo combinado conosco o serviço de plantio)
            </br>Mão de obra do plantio: por conta do comprador ou à combinar conosco.
            </br>Garantia de pega: por conta do comprador (obrigatória a irrigação diária durante 30 dias).
            </br>Acesso: estrada ruim, subidas fortes, acesso na obra , transbordo de cargas(se necessário) ,por conta do comprador.</p>

        <h3>Emissão de NF Produtor Rural</h3>
        <h5>Informações Adicionais:</h5>
        <p>- Orçamento válido por 7 dias.
            </br>- Plantar os tapetes de grama em 3 dias no máximo, evitando estocá-los;
            </br>- Programe a entrega da grama somente após ter preparado o solo para o plantio;
            </br>- Descarregue a grama com cuidado, evitando ao máximo a quebra ou danificação dos tapetes;
            </br>- Recebendo a grama confira a qualidade e a quantidade do produto e qualquer problema comunique-nos imediatamente pois nossos fretes são terceirizados e não aceitamos reclamações posteriores.
            </br>-A descarga tem que ser feita no máximo em 5 horas a partir da chegada ao destino, após isso será por conta do comprador o pagamento da estadia do caminhão de acordo com o § 5º do Art. 11 da Lei 11.442/07, alterado pela Lei 13.103/2015.</p>
    </div>

    <footer style="text-align:center; margin-bottom: 5px;">
        <img src="../../../img/flowlet.png" style="width:15%" />
    </footer>
</body>

<script>
    window.print();
</script>

</html>