<?php
include_once('../../conn/index.php');
$id = $_GET['id_next'];
$edit = $_GET['edit'];
$ename = $_GET['ename'];

$sql = "SELECT * FROM equipment";
$res_equipment = mysqli_query($conn, $sql);
?>

<div class="form-row">
    <div class="form-group col-md-6">
        <select onchange="seleciona_equipamento(this,'<?= $edit ?>','<?= $ename ?>')" id="equipment_<?= $id . $ename ?>" name="equipment_<?= $id . $ename ?>" class="form-control" id="">
            <option value="">Selecione o equipamento</option>
            <?php while ($row = mysqli_fetch_array($res_equipment)) { ?>
                <option value="<?= $row['id'] ?>"><?= $row['model'] . ' - ' . $row['brand'] ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="form-group col-md-4">
        <input type="number" id="evalue_<?= $id . $ename ?>" name="evalue_<?= $id . $ename ?>" class="form-control" readonly>
    </div>
    <div class="form-group col-md-2">
        <div class="btn btn-danger" onclick="remover_equipamento(<?= $id ?>,'<?= $edit ?>','<?= $ename ?>')">Remover</div>
    </div>
</div>