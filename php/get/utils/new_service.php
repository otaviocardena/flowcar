<?php
include_once('../../conn/index.php');
$id = $_GET['id_next'];
$edit = $_GET['edit'];
$ename = $_GET['ename'];

$sql = "SELECT * FROM services WHERE status = 1";
$res_services = mysqli_query($conn, $sql);
?>

<div class="form-row">
    <div class="form-group col-md-5">
        <select onchange="seleciona_services(this,'<?= $edit ?>','<?= $ename ?>')" name="service_<?= $id . $ename ?>" id="service_<?= $id . $ename ?>" class="form-control">
            <option value="">Selecione um</option>
            <?php while ($row = mysqli_fetch_array($res_services)) { ?>
                <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="form-group col-md-5">
        <input id="service_value_<?= $id . $ename ?>" name="service_value_<?= $id . $ename ?>" type="number" class="form-control" readonly>
    </div>
    <div class="form-group col-md-2">
        <div class="btn btn-danger" onclick="remover_servico(<?= $id ?>, '<?= $edit ?>','<?= $ename ?>')">Remover</div>
    </div>
</div>