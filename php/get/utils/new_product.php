<?php
include_once('../../conn/index.php');
$id = $_GET['id_next'];
$edit = $_GET['edit'];
$ename = $_GET['ename'];

$sql = "SELECT * FROM products";
$res_products = mysqli_query($conn, $sql);
?>

<div class="form-row">
    <div class="form-group col-md-4">
        <select onchange="seleciona_produto(this,'<?= $edit ?>','<?= $ename ?>')" id="product_<?= $id . $ename ?>" name="product_<?= $id . $ename ?>" class="form-control" id="">
            <option value="">Selecione o produto</option>
            <?php while ($row = mysqli_fetch_array($res_products)) { ?>
                <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="form-group col-md-2">
        <input onchange="atualiza_custos('<?= $edit ?>','<?= $ename ?>')" type="number" step="0.01" id="qtd_product_<?= $id . $ename ?>" name="qtd_product_<?= $id . $ename ?>" class="form-control">
    </div>
    <div class="form-group col-md-2">
        <input type="text" id="type_product_<?= $id . $ename ?>" name="type_product_<?= $id . $ename ?>" class="form-control" readonly>
    </div>
    <div class="form-group col-md-2">
        <input type="text" id="pvalue_<?= $id . $ename ?>" name="pvalue_<?= $id . $ename ?>" class="form-control" readonly>
    </div>
    <div class="form-group col-md-2">
        <div class="btn btn-danger" onclick="remover_produto(<?= $id ?>,'<?= $edit ?>','<?= $ename ?>')">Remover</div>
    </div>
</div>