<?php
include_once('../../conn/index.php');

$id = $_GET['id'];

$sql = "SELECT * FROM sales WHERE id = $id";
$res_sales = mysqli_query($conn, $sql);

while ($row = mysqli_fetch_array($res_sales)) {
    $client_id = $row['client_id'];
    $responsible_name = $row['responsible_name'];
    $document_type = $row['document_type'];
    $document = $row['document'];
    $license_plate = $row['license_plate'];
    $payment_date = $row['payment_date'];
    $sale_obs = $row['sale_obs'];
    $discount = $row['discount'];
    $total_value = $row['total_value'];
    $schedule_date = $row['schedule_date'];
    $schedule_time = $row['schedule_time'];
    $car_milage = $row['car_milage'];
    $car_fuel = $row['car_fuel'];
    $driver_obs = $row['driver_obs'];
    $passenger_obs = $row['passenger_obs'];
    $front_obs = $row['front_obs'];
    $back_obs = $row['back_obs'];
    $ceiling_obs = $row['ceiling_obs'];
    $img_1 = $row['img_1'];
    $img_2 = $row['img_2'];
    $img_3 = $row['img_3'];
    $img_4 = $row['img_4'];
}

$cpf_show = '';
$cnpj_show = '';
$document_cnpj = '';
$document_cpf = '';

if ($document_type == 'cpf') {
    $cpf_show = 'show';
    $cnpj_show = 'hide';
    $document_cpf = $document;
    $document_cnpj = "";
} else {
    $cpf_show = 'hide';
    $cnpj_show = 'show';
    $document_cnpj = $document;
    $document_cpf = "";
}

$sql = "SELECT 
            ss.service_id,
            s.sale_value 
        FROM sales_services AS ss
        INNER JOIN services AS s ON
            s.id = ss.service_id
        WHERE sale_id = $id ";
$res_sales_services = mysqli_query($conn, $sql);
$qtd_services_view = mysqli_num_rows($res_sales_services);

$servicos = "";

$cont = "1";
?>


<div class="form-row">
    <div class="form-group col-md-3">
        <label for="document_type">Tipo de Cadastro</label>
        <select id="document_type_view" name="document_type_view" class="form-control" disabled>
            <option value="cpf">CPF</option>
            <option value="cnpj">CNPJ</option>
        </select>
    </div>

    <div class="form-group col-md-3 <?= $cpf_show ?>" id="div_cpf">
        <label for="cpf_view">CPF</label>
        <input id="cpf_view" name="cpf_view" type="text" class="form-control" value="<?= $document_cpf ?>" disabled>
    </div>
    <div class="form-group col-md-3 <?= $cnpj_show ?>" id="div_cnpj">
        <label for="cnpj">CNPJ</label>
        <input id="cnpj_view" name="cnpj_view" type="text" value="<?= $document_cnpj ?>" class="form-control" disabled>
    </div>
    <div class="form-group col-md-3">
        <label for="client_name_view">Nome do Cliente</label>
        <input id="client_name_view" name="client_name_view" type="text" class="form-control" value="<?= $responsible_name ?>" disabled>
    </div>
    <div class="form-group col-md-3">
        <label for="license_plate_view">Placa Veículo</label>
        <input id="license_plate_view" name="license_plate_view" type="text" class="form-control" maxlength="7" value='<?= $license_plate ?>' disabled>
    </div>
</div>
<div class="form-row">
    <div class="col-md-6">
        <label for="services_view">Serviço</label>
    </div>
    <div class="col-md-6">
        <label for="total_value_view">Valor Serviço</label>
    </div>
</div>
<div id="div-services-view">
    <?php while ($row = mysqli_fetch_array($res_sales_services)) {
        $servicos .= $row['service_id'] . ","; ?>
        <div id="div-service-<?= $cont ?>-view" class="divs-more">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <select name="service_<?= $cont ?>_view" id="service_<?= $cont ?>_view" class="form-control" disabled>
                        <option value="">Selecione um</option>
                        <?php
                        $sql = "SELECT * FROM services";
                        $res_services_view = mysqli_query($conn, $sql);
                        while ($rows = mysqli_fetch_array($res_services_view)) { ?>
                            <option value="<?= $rows['id'] ?>"><?= $rows['name'] ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <input id="service_value_<?= $cont ?>_view" name="service_value_<?= $cont ?>_view" type="number" class="form-control" min="0" value="<?= $row['sale_value'] ?>" disabled>
                </div>
            </div>
        </div>
    <?php $cont++;
    } ?>
</div>
<hr>
<div class="form-row">
    <div class="form-group col -md-4">
        <label for="payment_date_view">Dt Pagamento</label>
        <input id="payment_date_view" name="payment_date_view" type="date" class="form-control" value="<?= date('Y-m-d', strtotime($payment_date)); ?>" disabled>
    </div>
    <div class="form-group col-md-4">
        <label for="total_value_view">Valor Total</label>
        <input id="total_value_view" name="total_value_view" type="number" class="form-control" min="0" value="<?= $total_value ?>" disabled>
    </div>
    <div class="form-group col-md-4">
        <label for="discount_view">Desconto</label>
        <input id="discount_view" name="discount_view" type="number" class="form-control" min="0" value="<?= $discount ?>" disabled>
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-12">
        <label for="sale_obs_view">Observação</label>
        <textarea id="sale_obs_view" name="sale_obs_view" rows="3" class="form-control" disabled><?= $sale_obs ?></textarea>
    </div>
</div>

<script>
    var qtd_services_view = parseInt(<?= $qtd_services_view ?>);
    $('#document_type_view').val('<?= $document_type ?>');

    var servicos = '<?= $servicos ?>';
    servicos = servicos.split(",");
    // remover o ultimo vazio
    servicos.pop();

    for (var i = 0; i < servicos.length; i++) {
        var indice_div = i + 1;
        $('#service_' + indice_div + "_view").val(servicos[i]);
    }
</script>