<?php
include_once('../../conn/index.php');

$id = $_GET['id'];

$sql = "SELECT * FROM sales WHERE id = $id";
$res_sales = mysqli_query($conn, $sql);

while ($row = mysqli_fetch_array($res_sales)) {
    $client_id = $row['client_id'];
    $responsible_name = $row['responsible_name'];
    $document_type = $row['document_type'];
    $document = $row['document'];
    $license_plate = $row['license_plate'];
    $payment_date = $row['payment_date'];
    $sale_obs = $row['sale_obs'];
    $discount = $row['discount'];
    $total_value = $row['total_value'];
    $schedule_date = $row['schedule_date'];
    $schedule_time = $row['schedule_time'];
    $car_milage = $row['car_milage'];
    $car_fuel = $row['car_fuel'];
    $driver_obs = $row['driver_obs'];
    $passenger_obs = $row['passenger_obs'];
    $front_obs = $row['front_obs'];
    $back_obs = $row['back_obs'];
    $ceiling_obs = $row['ceiling_obs'];
    $img_1 = $row['img_1'];
    $img_2 = $row['img_2'];
    $img_3 = $row['img_3'];
    $img_4 = $row['img_4'];
}

$cpf_show = '';
$cnpj_show = '';
$document_cnpj = '';
$document_cpf = '';

if ($document_type == 'cpf') {
    $cpf_show = 'show';
    $cnpj_show = 'hide';
    $document_cpf = $document;
    $document_cnpj = "";
} else {
    $cpf_show = 'hide';
    $cnpj_show = 'show';
    $document_cnpj = $document;
    $document_cpf = "";
}

$cont = 1;

$sql = "SELECT 
            ss.service_id,
            s.sale_value 
        FROM sales_services AS ss
        INNER JOIN services AS s ON
            s.id = ss.service_id
        WHERE sale_id = $id ";
$res_sales_services = mysqli_query($conn, $sql);
$qtd_services_edit = mysqli_num_rows($res_sales_services);

$servicos = "";

?>


<div class="form-row">
    <div class="form-group col-md-3">
        <label for="document_type">Tipo de Cadastro</label>
        <select onchange="seleciona_tipo(this)" id="document_type_edit" name="document_type_edit" class="form-control">
            <option value="cpf">CPF</option>
            <option value="cnpj">CNPJ</option>
        </select>
    </div>
    <input type="hidden" id="qtd_services_edit" name="qtd_services_edit" value="<?= $qtd_services ?>">
    <input type="hidden" id="sale_id_edit" name="sale_id_edit" value="<?= $id ?>">
    <div class="form-group col-md-3 <?= $cpf_show ?>" id="div_cpf">
        <label for="cpf_edit">CPF</label>
        <input onchange="verificar_documento(this)" id="cpf_edit" name="cpf_edit" type="text" class="form-control" value="<?= $document_cpf ?>">
    </div>
    <div class="form-group col-md-3 <?= $cnpj_show ?>" id="div_cnpj">
        <label for="cnpj">CNPJ</label>
        <input onchange="verificar_documento(this)" id="cnpj_edit" name="cnpj_edit" type="text" value="<?= $document_cnpj ?>" class="form-control">
    </div>
    <div class="form-group col-md-3">
        <label for="client_name_edit">Nome do Cliente</label>
        <input id="client_name_edit" name="client_name_edit" type="text" class="form-control" value="<?= $responsible_name ?>" required>
    </div>
    <div class="form-group col-md-3">
        <label for="license_plate_edit">Placa Veículo</label>
        <input onchange="validaPlaca(this)" onkeydown="upperCase(this)" id="license_plate_edit" name="license_plate_edit" type="text" class="form-control" maxlength="7" value='<?= $license_plate ?>' required>
    </div>
</div>
<div class="form-row">
    <div class="col-md-5">
        <label for="services_edit">Serviço</label>
    </div>
    <div class="col-md-5">
        <label for="total_value_edit">Valor Serviço</label>
    </div>
    <div class="col-md-2">
        <label>Mais</label>
    </div>
</div>
<div id="div-services-edit">
    <?php while ($row = mysqli_fetch_array($res_sales_services)) {
        $servicos .= $row['service_id'] . ","; ?>
        <div id="div-service-<?= $cont ?>-edit" class="divs-more">
            <div class="form-row">
                <div class="form-group col-md-5">
                    <select onchange="seleciona_services(this,'-edit','_edit')" name="service_<?= $cont ?>_edit" id="service_<?= $cont ?>_edit" class="form-control" required>
                        <option value="">Selecione um</option>
                        <?php
                        $sql = "SELECT * FROM services";
                        $res_services_edit = mysqli_query($conn, $sql);
                        while ($rows = mysqli_fetch_array($res_services_edit)) { ?>
                            <option value="<?= $rows['id'] ?>"><?= $rows['name'] ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group col-md-5">
                    <input id="service_value_<?= $cont ?>_edit" name="service_value_<?= $cont ?>_edit" type="number" class="form-control" min="0" value="<?= $row['sale_value'] ?>" readonly>
                </div>
                <div class="form-group col-md-2">
                    <?php if ($cont == 1) { ?>
                        <div class="btn btn-primary" onclick="novo_servico('-edit', '_edit')">Adicionar</div>
                    <?php } else { ?>
                        <div class="btn btn-danger" onclick="remover_servico(<?= $cont ?>,'-edit', '_edit')">Remover</div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php $cont++;
    } ?>
</div>
<hr>
<div class="form-row">
    <div class="form-group col -md-4">
        <label for="payment_date_edit">Dt Pagamento</label>
        <input id="payment_date_edit" name="payment_date_edit" type="date" class="form-control" value="<?= date('Y-m-d', strtotime($payment_date)); ?>" required>
    </div>
    <div class="form-group col-md-4">
        <label for="total_value_edit">Valor Total</label>
        <input id="total_value_edit" name="total_value_edit" type="number" class="form-control" min="0" value="<?= $total_value ?>" readonly>
    </div>
    <div class="form-group col-md-4">
        <label for="discount_edit">Desconto</label>
        <input onchange="subtrairDesconto('_edit')" id="discount_edit" name="discount_edit" type="number" class="form-control" min="0" value="<?= $discount ?>" required>
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-12">
        <label for="sale_obs_edit">Observação</label>
        <textarea id="sale_obs_edit" name="sale_obs_edit" rows="3" class="form-control" required><?= $sale_obs ?></textarea>
    </div>
</div>

<script>
    var qtd_services_edit = parseInt(<?= $qtd_services_edit ?>);
    $('#document_type_edit').val('<?= $document_type ?>');

    var servicos = '<?= $servicos ?>';
    servicos = servicos.split(",");
    // remover o ultimo vazio
    servicos.pop();

    for (var i = 0; i < servicos.length; i++) {
        var indice_div = i + 1;
        $('#service_' + indice_div + "_edit").val(servicos[i]);
    }

    soma_custo_servicos = parseFloat(<?= $total_value ?>);
</script>