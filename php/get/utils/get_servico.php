<?php
include_once('../../conn/index.php');

$service_id = $_GET['id'];

$sql = "SELECT * FROM services WHERE id = $service_id";
$res_services = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res_services)) {
    $name = $row['name'];
    $description = $row['description'];
    $hours_amount = $row['hours_amount'];
    $hours_cost = $row['hours_cost'];
    $products_cost = $row['products_cost'];
    $equipment_cost = $row['equipment_cost'];
    $infrastructure_cost = $row['infrastructure_cost'];
    $total_cost = $row['total_cost'];
    $sale_value = $row['sale_value'];
    $profit_value = $row['profit_value'];
}

$sql = "SELECT 
            sp.product_id,
            sp.product_amount,
            p.measurement,
            p.cost_value,
            p.quantity
        FROM services_products AS sp
        INNER JOIN products AS p ON
            p.id = sp.product_id
        WHERE sp.service_id = $service_id";
$res_services_products = mysqli_query($conn, $sql);
$qtd_products_edit = mysqli_num_rows($res_services_products);

$sql = "SELECT 
            se.equipment_id,
            e.cost_value 
        FROM services_equipment AS se
        INNER JOIN equipment AS e ON
            e.id = se.equipment_id
        WHERE se.service_id = $service_id";
$res_services_equipment = mysqli_query($conn, $sql);
$qtd_equipment_edit = mysqli_num_rows($res_services_equipment);

$cont = 1;
$produtos = "";
$equipamentos = "";
?>
<div class="form-row">
    <div class="form-group col-md-12">
        <label for="name_edit">Nome do Serviço</label>
        <input type="text" id="name_edit" name="name_edit" class="form-control" value="<?= $name ?>" required>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-12">
        <label for="description_edit">Descrição</label>
        <textarea rows="3" type="text" id="description_edit" name="description_edit" class="form-control" required><?= $description ?></textarea>
    </div>
</div>
<input type="hidden" id="service_id_edit" name="service_id_edit" value="<?= $service_id ?>">
<input type="hidden" id="qtd_products_edit" name="qtd_products_edit" value="<?= $qtd_products_edit ?>">
<input type="hidden" id="qtd_equipment_edit" name="qtd_equipment_edit" value="<?= $qtd_equipment_edit ?>">


<div class="form-row">
    <div class="col-md-4">
        <label>Produto</label>
    </div>
    <div class="col-md-2">
        <label>Quantidade</label>
    </div>
    <div class="col-md-2">
        <label>Tipo</label>
    </div>
    <div class="col-md-2">
        <label>Valor Produto</label>
    </div>
    <div class="col-md-2">
        <label>Mais</label>
    </div>
</div>

<div id="div-products-edit">
    <?php while ($row = mysqli_fetch_array($res_services_products)) {
        $produtos .= $row['product_id'] . ",";
        $pvalue = $row['cost_value'] / $row['quantity'];
    ?>
        <div id="div-product-<?= $cont ?>-edit" class="divs-more">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <select onchange="seleciona_produto(this,'-edit','_edit')" id="product_<?= $cont ?>_edit" name="product_<?= $cont ?>_edit" class="form-control" required>
                        <option value="">Selecione o produto</option>
                        <?php
                        $sql = "SELECT * FROM products";
                        $res_products = mysqli_query($conn, $sql);
                        while ($rowp = mysqli_fetch_array($res_products)) { ?>
                            <option value="<?= $rowp['id'] ?>"><?= $rowp['name'] ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <input onchange="altera_qtd_produto('-edit','_edit')" type="number" step="0.01" id="qtd_product_<?= $cont ?>_edit" name="qtd_product_<?= $cont ?>_edit" class="form-control" value="<?= $row['product_amount'] ?>" required>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" id="type_product_<?= $cont ?>_edit" name="type_product_<?= $cont ?>_edit" class="form-control" value="<?= $row['measurement'] ?>" readonly>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" id="pvalue_<?= $cont ?>_edit" name="pvalue_<?= $cont ?>_edit" class="form-control" value="<?= $pvalue ?>" readonly>
                </div>
                <div class="form-group col-md-2">
                    <?php if ($cont == 1) { ?>
                        <div class="btn btn-primary" onclick="novo_produto('-edit','_edit')">Adicionar</div>
                    <?php } else { ?>
                        <div class="btn btn-danger" onclick="remover_produto(<?= $cont ?>,'-edit','_edit')">Remover</div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php
        $cont++;
    } ?>
</div>
<hr>
<div class="form-row">
    <div class="col-md-6">
        <label>Equipamento</label>
    </div>
    <div class="col-md-4">
        <label>Valor Equipamento</label>
    </div>
    <div class="col-md-2">
        <label>Mais</label>
    </div>
</div>
<div id="div-equipment-edit">
    <?php
    $cont = 1;
    while ($row = mysqli_fetch_array($res_services_equipment)) {
        $equipamentos .= $row['equipment_id'] . ",";
    ?>
        <div id="div-equipment-<?= $cont ?>-edit" class="divs-more">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <select onchange="seleciona_equipamento(this,'-edit','_edit')" id="equipment_<?= $cont ?>_edit" name="equipment_<?= $cont ?>_edit" class="form-control" required>
                        <option value="">Selecione o equipamento</option>
                        <?php
                        $sql = "SELECT * FROM equipment";
                        $res_equipment = mysqli_query($conn, $sql);
                        while ($rowe = mysqli_fetch_array($res_equipment)) { ?>
                            <option value="<?= $rowe['id'] ?>"><?= $rowe['model'] . ' - ' . $rowe['brand'] ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <input type="number" id="evalue_<?= $cont ?>_edit" name="evalue_<?= $cont ?>_edit" class="form-control" value="<?= $row['cost_value'] ?>" readonly>
                </div>
                <div class="form-group col-md-2">
                    <?php if ($cont == 1) { ?>
                        <div class="btn btn-primary" onclick="novo_equipamento('-edit','_edit')">Adicionar</div>
                    <?php } else { ?>
                        <div class="btn btn-danger" onclick="remover_equipamento(<?= $cont ?>,'-edit','_edit')">Remover</div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php
        $cont++;
    } ?>
</div>
<hr>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="hours_qtd_edit">Qtd Horas</label>
        <input min="1" onchange="atualiza_custos('-edit','_edit')" type="number" step="1" id="hours_qtd_edit" name="hours_qtd_edit" class="form-control" value="<?= $hours_amount ?>" required>
    </div>
    <div class="form-group col-md-6">
        <label for="hours_cost_edit">Valor Hora</label>
        <input type="number" step="0.01" id="hours_cost_edit" name="hours_cost_edit" class="form-control" value="<?= $hours_cost ?>" readonly>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-3">
        <label for="products_cost_edit">Custo Produtos</label>
        <input onchange="atualiza_custos('-edit','_edit')" type="number" id="products_cost_edit" name="products_cost_edit" value="<?= $products_cost ?>" class="form-control" readonly>
    </div>
    <div class="form-group col-md-3">
        <label for="equipment_cost_edit">Custo Equipamentos</label>
        <input onchange="atualiza_custos('-edit','_edit')" type="number" step="0.01" id="equipment_cost_edit" name="equipment_cost_edit" class="form-control" value="<?= $equipment_cost ?>" readonly>
    </div>
    <div class="form-group col-md-3">
        <label for="infra_cost">Custo Infraestrutura</label>
        <input type="number" step="0.01" id="infra_cost_edit" name="infra_cost_edit" class="form-control" value="<?= $infrastructure_cost ?>" readonly>
    </div>
    <div class="form-group col-md-3">
        <label for="total_cost">Custo Total</label>
        <input type="number" id="total_cost_edit" name="total_cost_edit" class="form-control" value="<?= $total_cost ?>" readonly>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="sale_value">Valor Venda</label>
        <input onchange="atualiza_custos('-edit','_edit')" step="0.01" type="number" id="sale_value_edit" name="sale_value_edit" class="form-control" value="<?= $sale_value ?>" required>
    </div>
    <div class="form-group col-md-6">
        <label for="profit_value">Valor Lucro</label>
        <input type="number" id="profit_value_edit" name="profit_value_edit" class="form-control" value="<?= $profit_value ?>" readonly>
    </div>
</div>

<script>
    var qtd_produtos_edit = parseInt(<?= $qtd_products_edit ?>);
    var qtd_equipamentos_edit = parseInt(<?= $qtd_equipment_edit ?>);

    var produtos = '<?= $produtos ?>';
    produtos = produtos.split(",");
    // remover o ultimo vazio
    produtos.pop();

    for (var i = 0; i < produtos.length; i++) {
        var indice_div = i + 1;
        $('#product_' + indice_div + "_edit").val(produtos[i]);
    }

    var equipamentos = '<?= $equipamentos ?>';
    equipamentos = equipamentos.split(",");
    // remover o ultimo vazio
    equipamentos.pop();

    for (var i = 0; i < equipamentos.length; i++) {
        var indice_div = i + 1;
        $('#equipment_' + indice_div + "_edit").val(equipamentos[i]);
    }

    soma_custo_produtos = parseFloat(<?= $products_cost ?>);
    soma_custo_equipamentos = parseFloat(<?= $equipment_cost ?>);
</script>