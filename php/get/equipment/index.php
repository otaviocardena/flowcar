<?php
include_once("../../conn/index.php");

$id = $_GET['id'];

$sql = "SELECT * FROM equipment WHERE id = $id";
$res = mysqli_query($conn, $sql);

$data = array();
while ($row = mysqli_fetch_array($res)) {
    array_push($data, array(
        'model' => $row['model'], 
        'brand' => $row['brand'],
        'cost_value' => $row['cost_value'],
    ));
}

mysqli_close($conn);
$json = json_encode($data);
echo $json;
