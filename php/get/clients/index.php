<?php
include_once("../../conn/index.php");

$id = $_GET['id'];

$sql = "SELECT * FROM clients WHERE id = $id";
$res = mysqli_query($conn, $sql);

$data = array();
while ($row = mysqli_fetch_array($res)) {
    array_push($data, array(
        'corporate_name' => $row['corporate_name'], 
        'fantasy_name' => $row['fantasy_name'],
        'document_type' => $row['document_type'],
        'document' => $row['document'],
        'email' => $row['email'],
        'zip' => $row['zip'],
        'street' => $row['street'],
        'number' => $row['number'],
        'neighborhood' => $row['neighborhood'],
        'city' => $row['city'],
        'state' => $row['state'],
        'complement' => $row['complement'],
        'phone' => $row['phone'],
        'cellphone' => $row['cellphone'],
        'birth_date' => $row['birth_date']
    ));
}

mysqli_close($conn);
$json = json_encode($data);
echo $json;
