<?php
include_once("../php/conn/index.php");

$sql = "SELECT * FROM equipment";
$res_equipment = mysqli_query($conn, $sql);

?>
<div class="container-fluid">
    <div class="card shadow mb-4" style="height: 100%;">
        <div class="card-header py-3" style="position: relative; display:flex">
            <h6 class="m-0 font-weight-bold text-primary">Consultar Equipamentos</h6>
            <div class="nav-search-btn">
                <button class="btn btn-primary" style="width: 100%;margin-right: 10px;border-radius: 25px;" data-toggle="modal" data-target="#cadastrarModal">
                    <i class="fas fa-plus"></i>
                    <span>Cadastrar Equipamento</span>
                </button>
            </div>
        </div>
        <div class="card-body">
            <div id="accordion" style="height:100%; overflow-y:scroll;width:100%;padding-right: 10px;">
                <table class="table" id="tableEquipamento">
                    <thead>
                        <tr>
                            <th scope="col">Modelo</th>
                            <th scope="col">Marca</th>
                            <th scope="col">Valor</th>
                            <th scope="col">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php while ($row = mysqli_fetch_array($res_equipment)) { ?>
                            <tr>
                                <td><?= $row['model'] ?></td>
                                <td><?= $row['brand'] ?></td>
                                <td>R$<?= number_format($row['cost_value'], 2, ',', '.') ?></td>
                                <td>
                                    <button onclick="edit(<?= $row['id'] ?>)" class="btn btn-primary" style="border-radius: 25px;padding: 6px 9px;" data-toggle="modal" data-target="#visualizarModal">
                                        <i class="far fa-eye"></i>
                                    </button>
                                    <?php if ($row['status'] == 1) { ?>
                                        <button type="button" onclick="on_off(<?= $row['id'] ?>,0)" class="btn btn-danger" style="border-radius: 25px;padding: 6px 12px;">
                                            <i class="fas fa-power-off"></i>
                                        </button>
                                    <?php } else if ($row['status'] == 0) { ?>
                                        <button type="button" onclick="on_off(<?= $row['id'] ?>,1)" class="btn btn-primary" style="border-radius: 25px;padding: 6px 12px;">
                                            <i class="fas fa-power-off"></i>
                                        </button>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="cadastrarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="padding:20px">
            <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h1>Cadastrar Equipamentos</h1>
            <form action="php/insert/equipment/" method="POST">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="model">Modelo</label>
                        <input type="text" id="model" name="model" class="form-control" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="brand">Marca</label>
                        <input type="text" id="brand" name="brand" class="form-control" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="cost_value">Valor</label>
                        <input type="number" step="0.01" id="cost_value" name="cost_value" class="form-control" required>
                    </div>
                </div>

                <div style="text-align-last: center;">
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="editaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="padding:20px">
            <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h1 style="color:#01B93C">Editar Equipamento</h1>
            <form action="php/update/equipment/edit.php" method="POST">
                <input type="hidden" id="id_equipment_edit" name="id_equipment_edit">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="model_edit">Modelo</label>
                        <input type="text" id="model_edit" name="model_edit" class="form-control" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="brand_edit">Marca</label>
                        <input type="text" id="brand_edit" name="brand_edit" class="form-control" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="cost_value_edit">Valor</label>
                        <input type="number" step="0.01" id="cost_value_edit" name="cost_value_edit" class="form-control" required>
                    </div>
                </div>
                <div style="text-align-last: center;">

                    <button class="btn btn-secondary" type="button" data-dismiss="modal" aria-label="Close">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Editar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- DESATIVAR -->
<div class="modal fade" id="modalDesativar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold" style="color: #01B93C;margin-left: 5px;" id="exampleModalLabel">Alerta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="php/update/equipment/onoff.php" method="POST">
        <input type="hidden" id="equipment_id_onoff" name="equipment_id_onoff">
        <input type="hidden" id="equipment_status" name="equipment_status">
        <div class="modal-body">
          Tem certeza que deseja alterar o status do equipamento <span style="color:#01B93C" id="equipment_onoff"></span>?<br>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-primary">Alterar</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
    $(document).ready(function() {
        $('#tableEquipamento').DataTable();
    });

    function edit(id) {
        $.get("php/get/equipment?id=" + id, function(data) {
            var json = JSON.parse(data)[0];
            $('equipment_id_edit').val(id);
            $('#model_edit').val(json.model);
            $('#brand_edit').val(json.brand);
            $('#cost_value_edit').val(json.cost_value);

            // console.log($('#editaModal'));
            $('#editaModal').modal('show');
        });
    }

    function on_off(id, status) {
        $('#equipment_id_onoff').val(id);
        $('#equipment_status').val(status);

        var nome_stt = status == 0 ? "para inativo" : "para ativo";
        $('#status_nome').html(nome_stt);

        $.get("php/get/equipment/index.php?id=" + id, function(data) {
            var json = JSON.parse(data)[0];

            $('#equipment_onoff').html(json.model);

            $('#modalDesativar').modal("show");
        });
    }
</script>