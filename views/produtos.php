<?php
include_once("../php/conn/index.php");

$sql = "SELECT * FROM products";
$res_products = mysqli_query($conn, $sql);
?>

<div class="container-fluid">
    <div class="card shadow mb-4" style="height: 100%;">
        <div class="card-header py-3" style="position: relative; display:flex">
            <h6 class="m-0 font-weight-bold text-primary">Consultar Produtos</h6>
            <div class="nav-search-btn">
                <button class="btn btn-primary" style="width: 100%;margin-right: 10px;border-radius: 25px;" data-toggle="modal" data-target="#cadastrarModal">
                    <i class="fas fa-plus"></i>
                    <span>Cadastrar Produto</span>
                </button>
            </div>
        </div>
        <div class="card-body">
            <div id="accordion" style="height:100%; overflow-y:scroll;width:100%;padding-right: 10px;">
                <table class="table" id="tableProduto">
                    <thead>
                        <tr>
                            <th scope="col">Nome do Produto</th>
                            <th scope="col">Valor</th>
                            <th scope="col">Qtd Embalagem</th>
                            <th scope="col">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php while ($row = mysqli_fetch_array($res_products)) {  ?>
                            <tr>
                                <td><?= $row['name'] ?></td>
                                <td><?= $row['cost_value'] ?></td>
                                <td><?= $row['quantity'] ?></td>
                                <td>
                                    <button onclick="edit(<?= $row['id'] ?>)" class="btn btn-primary" style="border-radius: 25px;padding: 6px 9px;" data-toggle="modal" data-target="#visualizarModal">
                                        <i class="far fa-eye"></i>
                                    </button>
                                    <?php if ($row['status'] == 1) { ?>
                                        <button type="button" onclick="on_off(<?= $row['id'] ?>,0)" class="btn btn-danger" style="border-radius: 25px;padding: 6px 12px;">
                                            <i class="fas fa-power-off"></i>
                                        </button>
                                    <?php } else if ($row['status'] == 0) { ?>
                                        <button type="button" onclick="on_off(<?= $row['id'] ?>,1)" class="btn btn-primary" style="border-radius: 25px;padding: 6px 12px;">
                                            <i class="fas fa-power-off"></i>
                                        </button>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="cadastrarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="padding:20px">
            <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h1>Cadastrar Produtos</h1>
            <form action="php/insert/products/" method="POST">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="name">Nome do Produto</label>
                        <input id="name" name="name" type="text" class="form-control" required>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="cost_value">Valor</label>
                        <input id="cost_value" name="cost_value" type="text" class="form-control" placeholder="R$" required>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="quantity">Qtd Embalagem</label>
                        <input id="quantity" name="quantity" type="number" class="form-control" required>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="measurement">Tipo Quantidade</label>
                        <select name="measurement" id="measurement" class="form-control" required>
                            <option value="">(Ml, Un, L, g, Kg)</option>
                            <option value="Ml">Ml</option>
                            <option value="Un">Un</option>
                            <option value="L">L</option>
                            <option value="g">g</option>
                            <option value="Kg">Kg</option>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="description">Descrição</label>
                        <textarea rows="3" id="description" name="description" type="text" class="form-control" placeholder="Digite Aqui" required></textarea>
                    </div>
                </div>

                <div style="text-align-last: center;">
                    <button type="submit" class="btn btn-primary">Cadastrar Produto</button>
                </div>

            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="editaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="padding:20px">
            <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h1 style="color:#01B93C">Editar Produto</h1>
            <form action="php/update/products/edit.php" method="POST">
                <input type="hidden" id="product_id_edit" name="product_id_edit">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="name_edit">Nome do Produto</label>
                        <input id="name_edit" name="name_edit" type="text" class="form-control" required>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="cost_value_edit">Valor</label>
                        <input id="cost_value_edit" name="cost_value_edit" type="text" class="form-control" placeholder="R$" required>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="quantity_edit">Qtd Embalagem</label>
                        <input id="quantity_edit" name="quantity_edit" type="number" class="form-control" required>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="measurement_edit">Tipo Quantidade</label>
                        <select name="measurement_edit" id="measurement_edit" class="form-control" required>
                            <option value="">(Ml, Un, L, g, Kg)</option>
                            <option value="Ml">Ml</option>
                            <option value="Un">Un</option>
                            <option value="L">L</option>
                            <option value="g">g</option>
                            <option value="Kg">Kg</option>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="description_edit">Descrição</label>
                        <textarea rows="3" id="description_edit" name="description_edit" type="text" class="form-control" placeholder="Digite Aqui" required></textarea>
                    </div>
                </div>
                <div style="text-align-last: center;">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal" aria-label="Close">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Editar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- DESATIVAR -->
<div class="modal fade" id="modalDesativar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" style="color: #01B93C;margin-left: 5px;" id="exampleModalLabel">Alerta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="php/update/products/onoff.php" method="POST">
                <input type="hidden" id="product_id_onoff" name="product_id_onoff">
                <input type="hidden" id="product_status" name="product_status">
                <div class="modal-body">
                    Tem certeza que deseja alterar o status do produto <span style="color:#01B93C" id="product_onoff"></span>?<br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Alterar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#tableProduto').DataTable();
    });

    function edit(id) {
        $.get("php/get/products?id=" + id, function(data) {
            var json = JSON.parse(data)[0];
            $('#product_id_edit').val(id);
            $('#name_edit').val(json.name);
            $('#cost_value_edit').val(json.cost_value);
            $('#quantity_edit').val(json.quantity);
            $('#measurement_edit').val(json.measurement);
            $('#description_edit').html(json.description);

            // console.log($('#editaModal'));
            $('#editaModal').modal('show');
        });
    }

    function on_off(id, status) {
        $('#product_id_onoff').val(id);
        $('#product_status').val(status);

        var nome_stt = status == 0 ? "para inativo" : "para ativo";
        $('#status_nome').html(nome_stt);

        $.get("php/get/products/index.php?id=" + id, function(data) {
            var json = JSON.parse(data)[0];

            $('#product_onoff').html(json.name);

            $('#modalDesativar').modal("show");
        });
    }
</script>