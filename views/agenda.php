<?php
include_once("../php/conn/index.php");

// Select scheduled
$sql = "SELECT * FROM sales WHERE status = 1 OR status = 3 OR status = 4";
$res_schedule = mysqli_query($conn, $sql);
?>
<div class="container-fluid">
    <div class="card shadow mb-4" style="height: 100%;">
        <div class="card-header py-3" style="position: relative; display:flex">
            <h6 class="m-0 font-weight-bold text-primary">Agenda</h6>
        </div>
        <div class="card-body">
            <div id="accordion" style="height:100%; overflow-y:scroll;width:100%;padding-right: 10px;">
                <table class="table" id="tableAgenda">
                    <thead>
                        <tr>
                            <th scope="col">Nome</th>
                            <th scope="col">Data Agenda</th>
                            <th scope="col">Horário</th>
                            <th scope="col">Placa</th>
                            <th scope="col">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php while ($row = mysqli_fetch_array($res_schedule)) {
                            $time = explode(":", $row['schedule_time']);
                            $time = $time[0] . ":" . $time[1];
                        ?>
                            <tr>
                                <td><?= $row['responsible_name'] ?></td>
                                <td><?= date('d/m/Y', strtotime($row['schedule_date'])) ?></td>
                                <td><?= $time ?></td>
                                <td><?= $row['license_plate'] ?></td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Ações
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <?php if ($row['status'] == 1 || $row['status'] == 3) { ?>
                                                <a onclick="edit_agendamento(<?= $row['id'] ?>)" href="#" class="dropdown-item"><i class="fas fa-calculator" style="margin-right:5px;"></i>Editar Agendamento</a>
                                            <?php }
                                            if ($row['status'] == 1) { ?>
                                                <a onclick="checklist(<?= $row['id'] ?>)" href="#" class="dropdown-item"><i class="fas fa-clipboard-check" style="margin-right:5px;"></i>Cadastrar Checklist</a>
                                            <?php } else if ($row['status'] == 3) { ?>
                                                <a onclick="edit_checklist(<?= $row['id'] ?>)" href="#" class="dropdown-item"><i class="fas fa-clipboard-check" style="margin-right:5px;"></i>Editar Checklist</a>
                                                <a onclick="finalizar_agendamento(<?= $row['id'] ?>)" href="#" class="dropdown-item"><i class="fas fa-comments-dollar" style="margin-right:5px;"></i>Finalizar Agendamento</a>
                                                <a onclick="imprimir(<?= $row['id'] ?>)" class="dropdown-item" target="_blank" style="cursor:pointer" href="php/get/utils/get_impressao_checklist.php?id=<?= $row['id'] ?>"><i class="fas fa-print" style="margin-right:5px"></i>Imprimir</a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editaAgendamento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="padding:20px">
            <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h1 style="color:#01B93C">Editar Agendamento</h1>
            <form action="php/update/sales/schedule.php" method="POST">
                <input type="hidden" id="sale_id_to_schedule" name="sale_id_to_schedule">

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="schedule_time">Data Agendamento</label>
                        <input onchange="troca_datapicker(this)" type="date" id="schedule_date" name="schedule_date" class="form-control" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="schedule_time">Horario</label>
                        <input type="time" id="schedule_time" name="schedule_time" class="form-control" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12" style="text-align: -webkit-center;">
                        <div id="calendar"></div>
                    </div>
                </div>
                <hr>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div id="div-orc-view">

                        </div>
                    </div>
                </div>
                <div style="text-align-last: center;">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal" aria-label="Close">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Confirmar Agendamento</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="finalizaAgendamento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="padding:20px">
            <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h1 style="color:#01B93C">Finalizar Agendamento</h1>
            <form action="php/update/sales/finish.php" method="POST">
                <input type="hidden" id="sale_id_to_finish" name="sale_id_to_finish">

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div id="div-orc-view-finish">

                        </div>
                    </div>
                </div>
                <div style="text-align-last: center;">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal" aria-label="Close">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Gerar Conta a Receber</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalContaRec" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="padding:20px">
            <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h1 style="color:#01B93C">Cadastrar conta a receber</h1>
            <form>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="inputRazao">Nome do Cliente</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputRazao">Placa Veículo</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputRazao">Valor Final</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputRazao">Data pagamento</label>
                        <input type="date" class="form-control">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputRazao">Serviço</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputRazao">Valor Serviço</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputRazao">Serviço</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputRazao">Valor Serviço</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
                <hr>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputRazao">Dt Emissão</label>
                        <input type="date" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputRazao">Dt Agenda</label>
                        <input type="date" class="form-control">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="inputRazao">Observação</label>
                        <textarea name="" id="" class="form-control" cols="30" rows="10"></textarea>
                    </div>
                </div>
                <div style="text-align-last: center;">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal" aria-label="Close">Cancelar</button>
                    <button type="submit" class="btn btn-primary">
                        <i class="fas fa-edit"></i>
                        Cadastrar</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="modalChecklist" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="padding:20px">
            <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h1 style="color:#01B93C">Cadastrar Checklist</h1>
            <form action="php/update/sales/checklist.php" method="POST" enctype="multipart/form-data">
                <input type="hidden" id="sales_id" name="sales_id">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="client_name">Nome do Cliente</label>
                        <input type="text" id="client_name" name="client_name" class="form-control" readonly>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="license_plate">Placa Veículo</label>
                        <input type="text" id="license_plate" name="license_plate" class="form-control" readonly>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="car_milage">KM Rodados</label>
                        <input type="number" step="1" min="0" id="car_milage" name="car_milage" class="form-control" required>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="car_fuel">Combustível</label>
                        <input type="number" step="0.01" min="0" id="car_fuel" name="car_fuel" class="form-control" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="driver_obs">Observação Lateral Motorista</label>
                        <textarea rows="3" id="driver_obs" name="driver_obs" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="passenger_obs">Observação Lateral Passageiro</label>
                        <textarea rows="3" id="passenger_obs" name="passenger_obs" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="front_obs">Observação Frente</label>
                        <textarea rows="3" id="front_obs" name="front_obs" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="back_obs">Observação Traseira</label>
                        <textarea rows="3" id="back_obs" name="back_obs" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="ceiling_obs">Observação Teto</label>
                        <textarea rows="3" id="ceiling_obs" name="ceiling_obs" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label>Imagem 01</label>
                        <input onchange="mostrarimg(this)" type="file" id="img_1" name="img_1" class="hide">
                        <a id="false_input_1" class="btn btn-light btn-icon-split" style="position:relative">
                            <span id="icon_1" class="icon">
                                <i class="fas fa-cloud-download-alt"></i>
                            </span>
                            <!-- <div class="btn-information-inside">
                                <h5 class="m-0 font-weight-bold font-description">Imagem 01</h5>
                                <h5 class="m-0 font-weight-bold font-description">Indisponível</h5>
                            </div> -->
                            <img id="show_img_1" class="hide" style="position:relative; width:100%;height: 101px;">
                        </a>
                    </div>
                    <div class="form-group col-md-3">
                        <label>Imagem 02</label>
                        <input onchange="mostrarimg(this)" type="file" id="img_2" name="img_2" class="hide">
                        <a id="false_input_2" class="btn btn-light btn-icon-split" style="position:relative">
                            <span id="icon_2" class="icon">
                                <i class="fas fa-cloud-download-alt"></i>
                            </span>
                            <!-- <div class="btn-information-inside">
                                <h5 class="m-0 font-weight-bold font-description">Imagem 01</h5>
                                <h5 class="m-0 font-weight-bold font-description">Indisponível</h5>
                            </div> -->
                            <img id="show_img_2" class="hide" style="position:relative; width:100%;height: 101px;">
                        </a>
                    </div>
                    <div class="form-group col-md-3">
                        <label>Imagem 03</label>
                        <input onchange="mostrarimg(this)" type="file" id="img_3" name="img_3" class="hide">
                        <a id="false_input_3" class="btn btn-light btn-icon-split" style="position:relative">
                            <span id="icon_3" class="icon">
                                <i class="fas fa-cloud-download-alt"></i>
                            </span>
                            <!-- <div class="btn-information-inside">
                                <h5 class="m-0 font-weight-bold font-description">Imagem 01</h5>
                                <h5 class="m-0 font-weight-bold font-description">Indisponível</h5>
                            </div> -->
                            <img id="show_img_3" class="hide" style="position:relative; width:100%;height: 101px;">
                        </a>
                    </div>
                    <div class="form-group col-md-3">
                        <label>Imagem 04</label>
                        <input onchange="mostrarimg(this)" type="file" id="img_4" name="img_4" class="hide">
                        <a id="false_input_4" class="btn btn-light btn-icon-split" style="position:relative">
                            <span id="icon_4" class="icon">
                                <i class="fas fa-cloud-download-alt"></i>
                            </span>
                            <!-- <div class="btn-information-inside">
                                <h5 class="m-0 font-weight-bold font-description">Imagem 01</h5>
                                <h5 class="m-0 font-weight-bold font-description">Indisponível</h5>
                            </div> -->
                            <img id="show_img_4" class="hide" style="position:relative; width:100%;height: 101px;">
                        </a>

                    </div>
                </div>
                <div style="text-align-last: center;">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal" aria-label="Close">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Confirmar Checklist</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- DESATIVAR -->
<div class="modal fade" id="modalDesativar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" style="color: #01B93C;margin-left: 5px;" id="exampleModalLabel">Alerta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Tem certeza que deseja excluir o Agenda?<br>
                <span style="color:#01B93C">Nome Agenda</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary">Excluir</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#tableAgenda').DataTable();

        $('#false_input_1').click(function() {
            $('#img_1').click();
        });
        $('#false_input_2').click(function() {
            $('#img_2').click();
        });
        $('#false_input_3').click(function() {
            $('#img_3').click();
        });
        $('#false_input_4').click(function() {
            $('#img_4').click();
        });

        $('#calendar').datepicker({
            defaultDate: new Date(),
            inline: true,
            dateFormat: 'yy-mm-dd',
            firstDay: 1,
            showOtherMonths: true,
            dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            monthNames: [
                'Janeiro',
                'Fevereiro',
                'Março',
                'Abril',
                'Maio',
                'Junho',
                'Julho',
                'Agosto',
                'Setembro',
                'Outubro',
                'Novembro',
                'Dezembro'
            ],
            monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
            onSelect: function(dateText) {
                $('#schedule_date').val(dateText);
            }
        }, );

        $('#schedule_date').val('<?= date('Y-m-d') ?>');
    });

    function edit_agendamento(id) {
        $.get('php/get/sales?id=' + id, function(data) {
            var json = JSON.parse(data)[0];
            $('#schedule_date').val(json.schedule_date);
            $("#calendar").datepicker("setDate", json.schedule_date);
            $('#schedule_time').val(json.schedule_time);

            $.get('php/get/utils/view_orcamento.php?id=' + id, function(view) {
                $('#sale_id_to_schedule').val(id);
                $('#div-orc-view').html(view);

                $('#editaAgendamento').modal("show");
            });
        });
    }

    function checklist(id) {
        $.get('php/get/sales?id=' + id, function(data) {
            var json = JSON.parse(data)[0];
            $('#sales_id').val(id);
            $('#client_name').val(json.responsible_name);
            $('#license_plate').val(json.license_plate);
            $('#car_milage').val("");
            $('#car_fuel').val("");
            $('#driver_obs').val("");
            $('#passenger_obs').val("");
            $('#front_obs').val("");
            $('#back_obs').val("");
            $('#ceiling_obs').val("");

            $('#img_1').val("");
            $('#icon_1').addClass("show").removeClass("hide");
            $('#show_img_1').addClass("hide").removeClass("show");

            $('#img_2').val("");
            $('#icon_2').addClass("show").removeClass("hide");
            $('#show_img_2').addClass("hide").removeClass("show");

            $('#img_3').val("");
            $('#icon_3').addClass("show").removeClass("hide");
            $('#show_img_3').addClass("hide").removeClass("show");

            $('#img_4').val("");
            $('#icon_4').addClass("show").removeClass("hide");
            $('#show_img_4').addClass("hide").removeClass("show");

            $('#modalChecklist').modal("show");
        });
    }

    function mostrarimg(obj) {
        var file = document.getElementById(obj.id).files;
        var n = obj.id.split("_")[1];

        if (obj.id.split("_").length > 2) {
            var ename = "_" + n.split("_")[2];
        } else {
            var ename = "";
        }


        if (file.length > 0) {
            var reader = new FileReader();

            reader.onload = function(e) {
                document.getElementById('show_img_' + n + ename).setAttribute("src", e.target.result);
                $('#icon_' + n + ename).addClass("hide").removeClass("show");
                $('#show_img_' + n + ename).addClass("show").removeClass("hide");
            }

            reader.readAsDataURL(file[0]);
        }
    }

    function troca_datapicker(obj) {
        $("#calendar").datepicker("setDate", obj.value);
    }

    function edit_checklist(id) {
        $.get('php/get/sales?id=' + id, function(data) {
            var json = JSON.parse(data)[0];

            $('#sales_id').val(id);
            $('#client_name').val(json.responsible_name);
            $('#license_plate').val(json.license_plate);
            $('#car_milage').val(json.car_milage);
            $('#car_fuel').val(json.car_fuel);
            $('#driver_obs').val(json.driver_obs);
            $('#passenger_obs').val(json.passenger_obs);
            $('#front_obs').val(json.front_obs);
            $('#back_obs').val(json.back_obs);
            $('#ceiling_obs').val(json.ceiling_obs);

            $('#img_1').val("");
            if (json.img_1 != null && json.img_1 != "") {
                $('#icon_1').addClass("hide").removeClass("show");
                $('#show_img_1').addClass("show").removeClass("hide");
                document.getElementById('show_img_1').setAttribute("src", "data:image/png;base64," + json.img_1);
            } else {
                $('#img_1').val("");
                $('#icon_1').addClass("show").removeClass("hide");
                $('#show_img_1').addClass("hide").removeClass("show");
            }

            $('#img_2').val("");
            if (json.img_2 != null && json.img_2 != "") {
                $('#icon_2').addClass("hide").removeClass("show");
                $('#show_img_2').addClass("show").removeClass("hide");
                document.getElementById('show_img_2').setAttribute("src", "data:image/png;base64," + json.img_2);
            } else {
                $('#img_2').val("");
                $('#icon_2').addClass("show").removeClass("hide");
                $('#show_img_2').addClass("hide").removeClass("show");
            }

            $('#img_3').val("");
            if (json.img_3 != null && json.img_3 != "") {
                $('#icon_3').addClass("hide").removeClass("show");
                $('#show_img_3').addClass("show").removeClass("hide");
                document.getElementById('show_img_3').setAttribute("src", "data:image/png;base64," + json.img_3);
            } else {
                $('#img_3').val("");
                $('#icon_3').addClass("show").removeClass("hide");
                $('#show_img_3').addClass("hide").removeClass("show");
            }

            $('#img_4').val("");
            if (json.img_4 != null && json.img_4 != "") {
                $('#icon_4').addClass("hide").removeClass("show");
                $('#show_img_4').addClass("show").removeClass("hide");
                document.getElementById('show_img_4').setAttribute("src", "data:image/png;base64," + json.img_4);
            } else {
                $('#img_4').val("");
                $('#icon_4').addClass("show").removeClass("hide");
                $('#show_img_4').addClass("hide").removeClass("show");
            }

            $('#modalChecklist').modal("show");
        });
    }

    function finalizar_agendamento(id) {
        $.get('php/get/utils/view_orcamento.php?id=' + id, function(view) {
            $('#sale_id_to_finish').val(id);
            $('#div-orc-view-finish').html(view);

            $('#finalizaAgendamento').modal("show");
        });
    }
</script>