<div class="container-fluid">
    <div class="card shadow mb-4" style="height: 100%;">
        <div class="card-header py-3" style="position: relative;text-align:center">
            <h6 class="m-0 font-weight-bold text-primary">Dados da empresa</h6>

        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputRazao">Nome Fantasia</label>
                    <input type="text" class="form-control" id="nomeFantasia" disabled>
                </div>
                <div class="form-group col-md-4">
                    <label for="inputInscricao">Razão Social</label>
                    <input type="text" class="form-control" id="razaoSocial"disabled>
                </div>
                <div class="form-group col-md-4">
                    <label for="inputInscricao">CNPJ</label>
                    <input type="text" class="form-control" id="cnpj" disabled>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-9">
                    <label for="inputRazao">E-mail</label>
                    <input type="text" class="form-control" id="email" disabled>
                </div>
                <div class="form-group col-md-3">
                    <label for="inputInscricao">Telefone</label>
                    <input type="text" class="form-control" id="telefone" disabled>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputRazao">Endereço</label>
                    <input type="text" class="form-control" id="endereco" disabled>
                </div>
                <div class="form-group col-md-2">
                    <label for="inputInscricao">Número</label>
                    <input type="text" class="form-control" id="numero" disabled>
                </div>
                <div class="form-group col-md-3">
                    <label for="inputInscricao">Bairro</label>
                    <input type="text" class="form-control" id="bairro" disabled>
                </div>
                <div class="form-group col-md-3">
                    <label for="inputInscricao">CEP</label>
                    <input type="text" class="form-control" id="cep" disabled>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-5">
                    <label for="inputRazao">Cidade</label>
                    <input type="text" class="form-control" id="cidade" disabled>
                </div>
                <div class="form-group col-md-2">
                    <label for="uf">UF</label>
                    <input type="text" class="form-control" id="uf" disabled>
                </div>
                <div class="form-group col-md-5">
                    <label for="inputInscricao">Complemento</label>
                    <input type="text" class="form-control" id="complemento" disabled>
                </div>
            </div>
            <!-- <div style="text-align-last: center;">
                <button type="submit" class="btn btn-primary">
                    <i class="fas fa-edit"></i>
                    Editar</button>
            </div> -->
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $.get("php/get/enterprise/index.php", function(data) {
            var json = JSON.parse(data);
            $("#nomeFantasia").val(json[0].fantasy_name);
            $("#razaoSocial").val(json[1].corporate_name);
            $("#cnpj").val(json[2].document);
            $("#email").val(json[3].email);
            $("#cep").val(json[4].zip);
            $("#endereco").val(json[5].street);
            $("#numero").val(json[6].number);
            $("#bairro").val(json[7].neighborhood);
            $("#cidade").val(json[8].city);
            $("#uf").val(json[9].state);
            $("#complemento").val(json[10].complement);
            $("#telefone").val(json[11].phone);
        });
    });
</script>