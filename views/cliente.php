<?php
include_once("../php/conn/index.php");
$sql = "SELECT id,fantasy_name, status, document FROM clients";
$res = mysqli_query($conn, $sql);

if (isset($_GET['modal'])) {
  $modal = 'show';
} else {
  $modal = 'hide';
}
?>

<div class="container-fluid">
  <div class="card shadow mb-4" style="height: 100%;">
    <div class="card-header py-3" style="position: relative; display:flex">
      <h6 class="m-0 font-weight-bold text-primary">Consultar cliente</h6>
      <div class="nav-search-btn">
        <button class="btn btn-primary" style="width: 100%;margin-right: 10px;border-radius: 25px;" data-toggle="modal" data-target="#cadastrarModal">
          <i class="fas fa-plus"></i>
          <span>Cadastrar Cliente</span>
        </button>
      </div>
    </div>
    <div class="card-body">
      <table class="table" id="DataTableClientes">
        <thead>
          <tr>
            <th scope="col">Nome</th>
            <th scope="col">Documento</th>
            <th scope="col">Ações</th>
          </tr>
        </thead>
        <tbody>
          <?php while ($row = mysqli_fetch_array($res)) { ?>
            <tr>
              <td><?= $row['fantasy_name'] ?></td>
              <td><?= $row['document'] ?></td>
              <td>
                <button type="button" onclick="edit(<?= $row['id'] ?>)" class="btn btn-primary" style="border-radius: 25px;padding: 6px 9px;" data-toggle="modal" data-target="#editaModal">
                  <i class="far fa-eye"></i>
                </button>
                <?php if ($row['status'] == 1) { ?>
                  <button type="button" onclick="on_off(<?= $row['id'] ?>,0)" class="btn btn-danger" style="border-radius: 25px;padding: 6px 12px;">
                    <i class="fas fa-power-off"></i>
                  </button>
                <?php } else if ($row['status'] == 0) { ?>
                  <button type="button" onclick="on_off(<?= $row['id'] ?>,1)" class="btn btn-primary" style="border-radius: 25px;padding: 6px 12px;">
                    <i class="fas fa-power-off"></i>
                  </button>
                <?php } ?>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>



<div class="modal fade" id="cadastrarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="padding:20px">
      <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h1>Cadastrar Cliente</h1>
      <form action="php/insert/clients/index.php" method="POST">
        <div class="form-row">
          <div class="form-group col-md-3">
            <label for="document_type">Tipo de Cadastro</label>
            <select onchange="seleciona_tipo(this)" id="document_type" name="document_type" class="form-control" id="">
              <option value="cpf">CPF</option>
              <option value="cnpj">CNPJ</option>
            </select>
          </div>
          <div id="div_cpf" class="form-group col-md-3 show">
            <label for="cpf">CPF</label>
            <input onchange="verificar_documento(this)" id="cpf" name="cpf" type="text" placeholder="999.999.999-99" class="form-control" required>
          </div>
          <div id="div_cnpj" class="form-group col-md-3 hide">
            <label for="cnpj">CNPJ</label>
            <input onchange="verificar_documento(this)" onkeyup="digita_cnpj(event,this)" id="cnpj" name="cnpj" type="text" placeholder="99.999.999/9999-99" class="form-control">
          </div>
          <div id="div_fantasyName" class="form-group col-md-3 show">
            <label for="fantasy_name"><span id="campo_fantasy">Nome</span></label>
            <input id="fantasy_name" name="fantasy_name" type="text" placeholder="Nome" class="form-control" required>
          </div>
          <div id="div_birthDate" class="form-group col-md-3 show">
            <label for="birth_date">Data de nascimento</label>
            <input id="birth_date" name="birth_date" type="date" class="form-control" required>
          </div>
          <div id="div_corporateName" class="form-group col-md-3 hide">
            <label for="corporate_name">Razão Social</label>
            <input id="corporate_name" name="corporate_name" type="text" placeholder="Razão Social" class="form-control">
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="email">E-mail</label>
            <input id="email" name="email" type="email" placeholder="Email" class="form-control">
          </div>
          <div class="form-group col-md-3">
            <label for="phone">Telefone</label>
            <input id="phone" name="phone" type="text" placeholder="(99) 9999-9999" class="form-control">
          </div>
          <div class="form-group col-md-3">
            <label for="cellphone">Celular</label>
            <input id="cellphone" name="cellphone" type="text" placeholder="(99) 9 9999-9999" class="form-control" required>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-3">
            <label for="zip">CEP</label>
            <input onchange="busca_cep(this)" id="zip" name="zip" type="text" placeholder="99999-999" class="form-control" required>
          </div>
          <div class="form-group col-md-4">
            <label for="street">Endereço</label>
            <input id="street" name="street" type="text" placeholder="Rua" class="form-control" required>
          </div>
          <div class="form-group col-md-2">
            <label for="number">Número</label>
            <input id="number" name="number" type="number" placeholder="999" class="form-control" required>
          </div>
          <div class="form-group col-md-3">
            <label for="neighborhood">Bairro</label>
            <input id="neighborhood" name="neighborhood" type="text" placeholder="Bairro" class="form-control" required>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-5">
            <label for="city">Cidade</label>
            <input id="city" name="city" type="text" placeholder="Cidade" class="form-control" required>
          </div>
          <div class="form-group col-md-2">
            <label for="state">UF</label>
            <select id="state" name="state" placeholder="UF" class="form-control" required>
              <option value="AC">AC</option>
              <option value="AL">AL</option>
              <option value="AP">AP</option>
              <option value="AM">AM</option>
              <option value="BA">BA</option>
              <option value="CE">CE</option>
              <option value="DF">DF</option>
              <option value="ES">ES</option>
              <option value="GO">GO</option>
              <option value="MA">MA</option>
              <option value="MT">MT</option>
              <option value="MS">MS</option>
              <option value="MG">MG</option>
              <option value="PA">PA</option>
              <option value="PB">PB</option>
              <option value="PR">PR</option>
              <option value="PE">PE</option>
              <option value="PI">PI</option>
              <option value="RJ">RJ</option>
              <option value="RN">RN</option>
              <option value="RS">RS</option>
              <option value="RO">RO</option>
              <option value="RR">RR</option>
              <option value="SC">SC</option>
              <option value="SP">SP</option>
              <option value="SE">SE</option>
              <option value="TO">TO</option>
            </select>
          </div>
          <div class="form-group col-md-5">
            <label for="complement">Complemento</label>
            <input id="complement" name="complement" type="text" placeholder="Complemento" class="form-control">
          </div>
        </div>

        <div style="text-align-last: center;">
          <button type="submit" class="btn btn-primary">Cadastrar</button>
        </div>
      </form>
    </div>
  </div>
</div>


<div class="modal fade" id="editaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="padding:20px">
      <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h1>Editar Cliente</h1>
      <form action="php/update/clients/edit.php" method="POST">
        <input type="hidden" name="client_id_edit" id="client_id_edit">
        <input type="hidden" name="document_edit_old" id="document_edit_old">
        <div class="form-row">
          <div class="form-group col-md-3">
            <label for="document_type_edit">Tipo de Cadastro</label>
            <select onchange="seleciona_tipo(this)" id="document_type_edit" name="document_type_edit" class="form-control" id="">
              <option value="cpf">CPF</option>
              <option value="cnpj">CNPJ</option>
            </select>
          </div>
          <div id="div_cpf_edit" class="form-group col-md-3">
            <label for="cpf_edit">CPF</label>
            <input onchange="verificar_documento(this)" id="cpf_edit" name="cpf_edit" type="text" class="form-control" required>
          </div>
          <div id="div_cnpj_edit" class="form-group col-md-3">
            <label for="cnpj_edit">CNPJ</label>
            <input onchange="verificar_documento(this)" onkeyup="digita_cnpj(event,this)" id="cnpj_edit" name="cnpj_edit" type="text" class="form-control">
          </div>
          <div id="div_fantasyName_edit" class="form-group col-md-3">
            <label for="fantasy_name_edit"><span id="campo_fantasy_edit">Nome</span></label>
            <input id="fantasy_name_edit" name="fantasy_name_edit" type="text" class="form-control" required>
          </div>
          <div id="div_birthDate_edit" class="form-group col-md-3">
            <label for="birth_date_edit">Data de nascimento</label>
            <input id="birth_date_edit" name="birth_date_edit" type="date" class="form-control" required>
          </div>
          <div id="div_corporateName_edit" class="form-group col-md-3">
            <label for="corporate_name_edit">Razão Social</label>
            <input id="corporate_name_edit" name="corporate_name_edit" type="text" class="form-control">
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="email_edit">E-mail</label>
            <input id="email_edit" name="email_edit" type="email" class="form-control">
          </div>
          <div class="form-group col-md-3">
            <label for="phone_edit">Telefone</label>
            <input id="phone_edit" name="phone_edit" type="text" class="form-control">
          </div>
          <div class="form-group col-md-3">
            <label for="cellphone_edit">Celular</label>
            <input id="cellphone_edit" name="cellphone_edit" type="text" class="form-control" required>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-3">
            <label for="zip_edit">CEP</label>
            <input onchange="busca_cep(this)" id="zip_edit" name="zip_edit" type="text" class="form-control" required>
          </div>
          <div class="form-group col-md-4">
            <label for="street_edit">Endereço</label>
            <input id="street_edit" name="street_edit" type="text" class="form-control" required>
          </div>
          <div class="form-group col-md-2">
            <label for="number_edit">Número</label>
            <input id="number_edit" name="number_edit" type="number" class="form-control" required>
          </div>
          <div class="form-group col-md-3">
            <label for="neighborhood_edit">Bairro</label>
            <input id="neighborhood_edit" name="neighborhood_edit" type="text" class="form-control" required>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-5">
            <label for="city_edit">Cidade</label>
            <input id="city_edit" name="city_edit" type="text" class="form-control" required>
          </div>
          <div class="form-group col-md-2">
            <label for="state_edit">UF</label>
            <select id="state_edit" name="state_edit" class="form-control" required>
              <option value="AC">AC</option>
              <option value="AL">AL</option>
              <option value="AP">AP</option>
              <option value="AM">AM</option>
              <option value="BA">BA</option>
              <option value="CE">CE</option>
              <option value="DF">DF</option>
              <option value="ES">ES</option>
              <option value="GO">GO</option>
              <option value="MA">MA</option>
              <option value="MT">MT</option>
              <option value="MS">MS</option>
              <option value="MG">MG</option>
              <option value="PA">PA</option>
              <option value="PB">PB</option>
              <option value="PR">PR</option>
              <option value="PE">PE</option>
              <option value="PI">PI</option>
              <option value="RJ">RJ</option>
              <option value="RN">RN</option>
              <option value="RS">RS</option>
              <option value="RO">RO</option>
              <option value="RR">RR</option>
              <option value="SC">SC</option>
              <option value="SP">SP</option>
              <option value="SE">SE</option>
              <option value="TO">TO</option>
            </select>
          </div>
          <div class="form-group col-md-5">
            <label for="complement_edit">Complemento</label>
            <input id="complement_edit" name="complement_edit" type="text" class="form-control">
          </div>
        </div>

        <div style="text-align-last: center;">
          <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancelar</button>
          <button type="submit" class="btn btn-primary">Editar</button>
        </div>
      </form>
    </div>
  </div>
</div>



<!-- DESATIVAR -->
<div class="modal fade" id="modalDesativar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold" style="color: #01B93C;margin-left: 5px;" id="exampleModalLabel">Alerta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="php/update/clients/onoff.php" method="POST">
        <input type="hidden" id="client_id_onoff" name="client_id_onoff">
        <input type="hidden" id="client_status" name="client_status">
        <div class="modal-body">
          Tem certeza que deseja alterar o status do cliente <span style="color:#01B93C" id="client_onoff"></span>?<br>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-primary">Alterar</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('#DataTableClientes').DataTable();
    $('#cpf').mask('999.999.999-99');
    $('#cpf_edit').mask('999.999.999-99');
    $('#cnpj').mask('99.999.999/9999-99');
    $('#cnpj_edit').mask('99.999.999/9999-99');
    $('#zip').mask('99999-999');
    $('#zip_edit').mask('99999-999');
    $('#phone').mask('(99) 9999-9999');
    $('#phone_edit').mask('(99) 9999-9999');
    $('#cellphone').mask('(99) 9 9999-9999');
    $('#cellphone_edit').mask('(99) 9 9999-9999');

    // proveniente do cadastro de cliente pela tela de orçamento
    var modal = '<?= $modal ?>';
    $('#cadastrarModal').modal(modal);
  });


  function seleciona_tipo(obj) {
    var complemento = obj.id.split("_");
    if (complemento.length > 2) {
      complemento = "_" + complemento[2];
    } else {
      complemento = "";
    }

    $('#div_cnpj' + complemento).removeClass("show").addClass("hide");
    $('#div_cpf' + complemento).removeClass("show").addClass("hide");

    $('#cnpj' + complemento).val("");
    $('#cpf' + complemento).val("");

    if (obj.value == "cnpj") {
      $('#cpf' + complemento).val("");
      $('#birth_date' + complemento).val("");
      document.getElementById('cnpj' + complemento).setAttribute("required", true);
      $('#cnpj' + complemento).removeClass('hide').addClass('show');

      document.getElementById('cpf' + complemento).removeAttribute("required");

      //Removendo data de aniversario e tirando o required
      $('#div_birthDate' + complemento).removeClass('show').addClass('hide');
      document.getElementById('birth_date' + complemento).removeAttribute("required");

      //Apresentando razao social e colocando required
      $('#div_corporateName' + complemento).removeClass('hide').addClass('show');
      document.getElementById('corporate_name' + complemento).setAttribute("required", true);

      //email
      document.getElementById('email' + complemento).setAttribute("required", true);


      //Trocando o nome de Nome para Nome Fantasia
      $('#campo_fantasy' + complemento).html("Nome Fantasia");
    } else {
      $('#cnpj' + complemento).val("");
      $('#corporate_name' + complemento).val("");
      document.getElementById('cpf' + complemento).setAttribute("required", true);
      document.getElementById('cnpj' + complemento).removeAttribute("required");

      //Data de Aniversario
      $('#div_birthDate' + complemento).removeClass('hide').addClass('show');
      document.getElementById('birth_date' + complemento).setAttribute("required", true);

      //Razao Social
      $('#div_corporateName' + complemento).removeClass('show').addClass('hide');
      document.getElementById('corporate_name' + complemento).removeAttribute('required');

      //email
      document.getElementById('email' + complemento).removeAttribute('required');

      $('#campo_fantasy' + complemento).html("Nome");
    }

    $('#div_' + obj.value + complemento).removeClass("hide").addClass("show");
    // console.log($('#div_' + obj.value + complemento));
  }

  async function digita_cnpj(e, obj) {
    // alert("entrou");
    var valor = obj.value.replace('.', "");
    valor = valor.replace('.', "");
    valor = valor.replace('/', "");
    valor = valor.replace('-', "");

    valor = valor.replace('_', "");
    // console.log(valor);

    // console.log(obj.id.split("_"));
    if (obj.id.split("_").length > 1) {
      var id_complemento = obj.id.split("_")[1];
      // console.log(id_complemento);
      id_complemento = "_" + id_complemento;
    } else {
      var id_complemento = "";
    }
    console.log(valor);
    console.log(valor.length);
    if (valor.length === 14) {
      await axios({
          method: 'GET',
          url: 'https://api.cnpja.com.br/companies/' + valor,
          data: {},
          headers: {
            "authorization": "0552132e-4140-4fa7-82f7-ab436ce169b3-463054c6-31fc-4624-8944-12b4b5abe1ee"
          }
        })
        .then(function(response) {

          // var formData = new FormData();
          console.log(response);

          var nome_fantasia = response.data.name.replace(/'/g, "´");
          var razao_social = response.data.name.replace(/'/g, "´");
          var email = response.data.email.replace(/'/g, "´");
          var endereco = response.data.address.street;
          var number = response.data.address.city;
          if (number == "SN") {
            number = 0;
          }
          var bairro = response.data.address.neighborhood;
          var cep = response.data.address.zip;
          cep = cep.slice(0, 5) + "-" + cep.slice(5);
          var cidade = response.data.address.city;
          var uf = response.data.address.state;
          var complemento = response.data.address.details;

          $('#fantasy_name' + id_complemento).val(nome_fantasia);
          $('#corporate_name' + id_complemento).val(razao_social);
          $('#email' + id_complemento).val(email);
          $('#street' + id_complemento).val(endereco);
          $('#number' + id_complemento).val(number);
          $('#neighborhood' + id_complemento).val(bairro);
          $('#zip' + id_complemento).val(cep);
          $('#city' + id_complemento).val(cidade);
          $('#state' + id_complemento).val(uf);
          $('#complement' + id_complemento).val(complemento);
        })
        .catch(function(response) {
          console.log(response);
        });
    }
  }

  function busca_cep(obj) {
    var complemento = obj.id.split("_");
    if (complemento.length > 1) {
      complemento = "_" + complemento[1];
    } else {
      complemento = "";
    }
    console.log(complemento);

    //Nova variável "cep" somente com dígitos.
    var cep = obj.value.replace(/\D/g, '');

    //Verifica se campo cep possui valor informado.
    if (cep != "") {

      //Expressão regular para validar o CEP.
      var validacep = /^[0-9]{8}$/;

      //Valida o formato do CEP.
      if (validacep.test(cep)) {

        //Consulta o webservice viacep.com.br/
        $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {

          if (!("erro" in dados)) {
            //Atualiza os campos com os valores da consulta.
            $('#street' + complemento).val(dados.logradouro);
            $('#neighborhood' + complemento).val(dados.bairro);
            $('#city' + complemento).val(dados.localidade);
            $('#state' + complemento).val(dados.uf);
            // $('#complemento').val(dados.complemento);
          } //end if.
          else {
            //CEP pesquisado não foi encontrado.
            alert("CEP não encontrado.");
          }
        });
      } //end if.
      else {
        alert("Formato de CEP inválido.");
      }
    }
  }

  function verificar_documento(obj) {
    var documento = obj.value;
    var tabela = "clients";

    var complemento = obj.id.split("_");
    var tipo = complemento[0];
    if (complemento.length > 1) {
      complemento = "_" + complemento[1];
      var documento_old = $('#document_edit_old').val();
    } else {
      complemento = "";
    }


    $.get("php/get/utils/verify_document.php?doc=" + documento + "&table=" + tabela, function(data) {
      if (parseInt(data) > 0) {
        if (documento_old) {
          // edit
          if (documento != documento_old) {
            if(tipo == 'cpf' && obj.value != ""){
              $('#cpf' + complemento).val("");
              alert("CPF já cadastrado!");
            }else if (obj.value != "") {
              $('#cnpj' + complemento).val("");
              alert("CNPJ já cadastrado!");
            }
          }
        } else {
          // cadastro
          if (obj.id == 'cpf' && obj.value != "") {
            $('#cpf' + complemento).val("");
            alert("CPF já cadastrado!");
          } else if (obj.value != "") {
            $('#cnpj' + complemento).val("");
            alert("CNPJ já cadastrado!");
          }
        }
      }
    });
  }

  function edit(id) {
    $('#div_birthDate_edit').removeClass('show').addClass('hide');
    $('#div_cpf_edit').removeClass('show').addClass('hide');
    $('#div_cnpj_edit').removeClass('show').addClass('hide');
    $.get("php/get/clients/index.php?id=" + id, function(data) {
      var json = JSON.parse(data)[0];
      $('#cnpj_edit').val("");
      $('#cpf_edit').val("");
      $('#client_id_edit').val(id);
      $('#fantasy_name_edit').val(json.fantasy_name);
      $('#document_type_edit').val(json.document_type);
      $('#document_edit_old').val(json.document);
      if (json.document_type == 'cpf') {
        $('#cnpj_edit').val("");
        $('#cpf_edit').val(json.document);
        $('#div_cpf_edit').removeClass('hide').addClass('show');
        $('#div_cnpj_edit').removeClass('show').addClass('hide');

        document.getElementById('cpf_edit').setAttribute("required", true);
        document.getElementById('cnpj_edit').removeAttribute("required");

        //Data de Aniversario
        $('#div_birthDate_edit').removeClass('hide').addClass('show');
        document.getElementById('div_birthDate_edit').setAttribute("required", true);

        //Razao Social
        $('#div_corporateName_edit').removeClass('show').addClass('hide');
        document.getElementById('corporate_name_edit').removeAttribute('required');

        document.getElementById('email_edit').removeAttribute('required');

        $('#campo_fantasy_edit').html("Nome");

        $('#birth_date_edit').val(json.birth_date);
      } else {
        $('#cpf_edit').val("");
        $('#div_cpf_edit').removeClass('show').addClass('hide');
        $('#div_cnpj_edit').removeClass('hide').addClass('show');

        $('#cnpj_edit').val(json.document);
        document.getElementById('cnpj_edit').setAttribute("required", true);
        $('#cnpj_edit').removeClass('hide').addClass('show');

        document.getElementById('cpf_edit').removeAttribute("required");

        //Removendo data de aniversario e tirando o required
        $('#div_birthDate_edit').removeClass('show').addClass('hide');
        document.getElementById('birth_date_edit').removeAttribute("required");

        //Apresentando razao social e colocando required
        $('#div_corporateName_edit').removeClass('hide').addClass('show');
        document.getElementById('corporate_name_edit').setAttribute("required", true);

        document.getElementById('email_edit').setAttribute("required", true);

        //Trocando o nome de Nome para Nome Fantasia
        $('#campo_fantasy_edit').html("Nome Fantasia");

        $('#corporate_name_edit').val(json.corporate_name);
      }
      $('#email_edit').val(json.email);
      $('#zip_edit').val(json.zip);
      $('#street_edit').val(json.street);
      $('#number_edit').val(json.number);
      $('#neighborhood_edit').val(json.neighborhood);
      $('#city_edit').val(json.city);
      $('#state_edit').val(json.state);
      $('#complement_edit').val(json.complement);
      $('#phone_edit').val(json.phone);
      $('#cellphone_edit').val(json.cellphone);

      // console.log($('#editaModal'));
      $('#editaModal').modal('show');
    });
  }

  function on_off(id, status) {
    $('#client_id_onoff').val(id);
    $('#client_status').val(status);

    var nome_stt = status == 0 ? "para inativo" : "para ativo";
    $('#status_nome').html(nome_stt);

    $.get("php/get/clients/index.php?id=" + id, function(data) {
      var json = JSON.parse(data)[0];

      if (json.document_type == 'cnpj') {
        $('#client_onoff').html(json.corporate_name);
      } else {
        $('#client_onoff').html(json.fantasy_name);
      }

      $('#modalDesativar').modal("show");
    });
  }
</script>