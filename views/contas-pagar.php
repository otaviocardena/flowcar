<?php
include_once("../php/conn/index.php");
$sql = "SELECT b.id,b.title,b.status,p.fantasy_name FROM bills_pay AS b LEFT JOIN providers AS p ON b.provider_id = p.id";
$res_bills_pay = mysqli_query($conn, $sql);

$sql = "SELECT id, fantasy_name FROM providers WHERE status = 1";
$res_providers = mysqli_query($conn, $sql);
$res_providers_2 = mysqli_query($conn, $sql);
?>

<div class="container-fluid">
  <div class="card shadow mb-4" style="height: 100%;">
    <div class="card-header py-3" style="position: relative; display:flex">
      <h6 class="m-0 font-weight-bold text-primary">Contas a pagar</h6>
      <div class="nav-search-btn">
        <button class="btn btn-primary" style="width: 100%;margin-right: 10px;border-radius: 25px;" data-toggle="modal" data-target="#cadastrarModal">
          <i class="fas fa-plus"></i>
          <span>Cadastrar contas a pagar</span>
        </button>
      </div>
    </div>
    <div class="card-body">
      <table class="table" id="DataTableClientes">
        <thead>
          <tr>
            <th scope="col">Título</th>
            <th scope="col">Status</th>
            <th scope="col">Fornecedor</th>
            <th scope="col">Ações</th>
          </tr>
        </thead>
        <tbody>
          <?php
          while ($row = mysqli_fetch_array($res_bills_pay)) {
            if ($row['status'] == 0) {
              $status = "Em aberto";
            } else if ($row['status'] == 1) {
              $status = "Paga";
            } else if ($row['status'] == 2) {
              $status = "Cancelada";
            }
          ?>
            <tr>
              <td><?= $row['title'] ?></td>
              <td><?= $status ?></td>
              <td><?= $row['fantasy_name'] ?></td>
              <td>
                <?php if ($row['status'] == 0) { ?>
                  <button type="button" onclick="edit(<?= $row['id'] ?>)" class="btn btn-primary" style="border-radius: 25px;padding: 6px 9px;" data-toggle="modal" data-target="#editaModal">
                    <i class="far fa-eye"></i>
                  </button>
                  <button type="button" onclick="on_off(<?= $row['id'] ?>)" class="btn btn-danger" data-toggle="modal" data-target="#modalDesativar" style="border-radius: 25px;padding: 6px 12px;">
                    <i class="fas fa-power-off"></i>
                  </button>
                  </button>
                <?php } else { ?>
                  <button type="button" onclick="visualiza(<?= $row['id'] ?>)" class="btn btn-primary" style="border-radius: 25px;padding: 6px 9px;" data-toggle="modal" data-target="#visualizaModal">
                    <i class="far fa-eye"></i>
                  </button>
                <?php } ?>
              </td>
            </tr>
        </tbody>
      <?php } ?>
      </table>
    </div>
  </div>
</div>



<div class="modal fade" id="cadastrarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="padding:20px">
      <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h1>Cadastrar nova conta a pagar</h1>
      <form action="php/insert/bills_pay/index.php" method="POST">
        <div class="form-row">
          <div class="form-group col-md-4">
            <label for="title">Título</label>
            <input id="title" name="title" type="text" placeholder="Título" class="form-control" required>
          </div>
          <div class="form-group col-md-8">
            <label for="description">Descrição</label>
            <input id="description" name="description" type="text" placeholder="Descrição" class="form-control" required>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="provider">Fornecedor</label>
            <select id="provider" name="provider" placeholder="Fornecedor" class="form-control" required>
              <?php while ($row = mysqli_fetch_array($res_providers)) { ?>
                <option value="<?= $row['id'] ?>"><?= $row['fantasy_name'] ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group col-md-6">
            <label for="bills_type">Tipo de Conta</label>
            <select name="bills_type" placeholder="Tipo Conta" class="form-control" id="bills_type" required>
              <option value="infraestrutura">Infraestrutura</option>
              <option value="produtos">Produtos</option>
              <option value="equipamento">Equipamento</option>
            </select>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-4">
            <label for="create_date">Data Emissão</label>
            <input id="create_date" name="create_date" type="date" class="form-control" required>
          </div>
          <div class="form-group col-md-4">
            <label for="payment_date">Data Pag.</label>
            <input id="payment_date" name="payment_date" type="date" class="form-control" required>
          </div>
          <div class="form-group col-md-4">
            <label for="payment_value">Valor</label>
            <input id="payment_value" name="payment_value" placeholder="R$" type="text" class="form-control" required>
          </div>
        </div>

        <div style="text-align-last: center;">
          <button type="submit" class="btn btn-primary">Cadastrar contas a pagar</button>
        </div>
      </form>
    </div>
  </div>
</div>


<div class="modal fade" id="editaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="padding:20px">
      <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h1>Validar conta a pagar</h1>
      <form action="php/update/bills_pay/edit.php" method="POST">
        <input type="hidden" name="bills_pay_id_edit" id="bills_pay_id_edit">
        <div class="form-row">
          <div class="form-group col-md-4">
            <label for="title_edit">Título</label>
            <input id="title_edit" name="title_edit" type="text" class="form-control" required>
          </div>
          <div class="form-group col-md-8">
            <label for="description_edit">Descrição</label>
            <input id="description_edit" name="description_edit" type="text" class="form-control" required>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="provider_edit">Fornecedor</label>
            <select id="provider_edit" name="provider_edit" class="form-control" required>
              <?php while ($row = mysqli_fetch_array($res_providers_2)) { ?>
                <option value="<?= $row['id'] ?>"><?= $row['fantasy_name'] ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group col-md-6">
            <label for="bills_type_edit">Tipo de Conta</label>
            <select name="bills_type_edit" class="form-control" id="bills_type_edit" required>
              <option value="infraestrutura">Infraestrutura</option>
              <option value="produtos">Produtos</option>
              <option value="equipamento">Equipamento</option>
            </select>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-4">
            <label for="create_date_edit">Data Emissão</label>
            <input id="create_date_edit" name="create_date_edit" type="date" class="form-control" required>
          </div>
          <div class="form-group col-md-4">
            <label for="payment_date_edit">Data Pag.</label>
            <input id="payment_date_edit" name="payment_date_edit" type="date" class="form-control" required>
          </div>
          <div class="form-group col-md-4">
            <label for="payment_value_edit">Valor</label>
            <input id="payment_value_edit" name="payment_value_edit" placeholder="R$" type="text" class="form-control" required>
          </div>
        </div>

        <div style="text-align-last: center;">
          <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancelar</button>
          <button type="submit" class="btn btn-primary">Validar conta a pagar</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="visualizaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="padding:20px">
      <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h1>Visualizar conta a pagar</h1>
      <input type="hidden" name="bills_pay_id_visualiza" id="bills_pay_id_visualiza">
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="title_visualiza">Título</label>
          <input id="title_visualiza" name="title_visualiza" type="text" class="form-control" disabled>
        </div>
        <div class="form-group col-md-8">
          <label for="description_visualiza">Descrição</label>
          <input id="description_visualiza" name="description_visualiza" type="text" class="form-control" disabled>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="provider_visualiza">Fornecedor</label>
          <input id="provider_visualiza" name="provider_visualiza" class="form-control" disabled>
        </div>
        <div class="form-group col-md-6">
          <label for="bills_type_visualiza">Tipo de Conta</label>
          <select name="bills_type_visualiza" class="form-control" id="bills_type_visualiza" disabled>
            <option value="infraestrutura">Infraestrutura</option>
            <option value="produtos">Produtos</option>
            <option value="equipamento">Equipamento</option>
          </select>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="create_date_visualiza">Data Emissão</label>
          <input id="create_date_visualiza" name="create_date_visualiza" type="date" class="form-control" disabled>
        </div>
        <div class="form-group col-md-4">
          <label for="payment_date_visualiza">Data Pag.</label>
          <input id="payment_date_visualiza" name="payment_date_visualiza" type="date" class="form-control" disabled>
        </div>
        <div class="form-group col-md-4">
          <label for="payment_value_visualiza">Valor</label>
          <input id="payment_value_visualiza" name="payment_value_visualiza" placeholder="R$" type="text" class="form-control" disabled>
        </div>
      </div>

      <div style="text-align-last: center;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Fechar</button>
      </div>
    </div>
  </div>
</div>



<!-- DESATIVAR -->
<div class="modal fade" id="modalDesativar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold" style="color: #01B93C;margin-left: 5px;" id="exampleModalLabel">Alerta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="php/update/bills_pay/onoff.php" method="POST">
        <input type="hidden" id="bills_pay_id_onoff" name="bills_pay_id_onoff">
        <div class="modal-body">
          Tem certeza que deseja alterar o status do cliente <span style="color:#01B93C" id="bills_pay_onoff"></span>?<br>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-primary">Alterar</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('#DataTableClientes').DataTable();

  });


  function edit(id) {
    $.get("php/get/bills_pay/index.php?id=" + id, function(data) {
      var json = JSON.parse(data)[0];
      $('#bills_pay_id_edit').val(id);
      $('#provider_edit').val(json.provider_id);
      $('#title_edit').val(json.title);
      $('#description_edit').val(json.description);
      $('#bills_type_edit').val(json.bills_type);
      $('#payment_value_edit').val(json.payment_value);
      $('#create_date_edit').val(json.create_date);
      $('#payment_date_edit').val(json.payment_date);

      // console.log($('#editaModal'));
      $('#editaModal').modal('show');
    });
  }

  function visualiza(id) {
    $.get("php/get/bills_pay/index.php?id=" + id, function(data) {
      var json = JSON.parse(data)[0];
      $('#bills_pay_id_visualiza').val(id);
      $('#provider_visualiza').val(json.fantasy_name);
      $('#title_visualiza').val(json.title);
      $('#description_visualiza').val(json.description);
      $('#bills_type_visualiza').val(json.bills_type);
      $('#payment_value_visualiza').val(json.payment_value);
      $('#create_date_visualiza').val(json.create_date);
      $('#payment_date_visualiza').val(json.payment_date);

      // console.log($('#editaModal'));
      $('#visualizaModal').modal('show');
    });
  }


  function on_off(id) {
    $('#bills_pay_id_onoff').val(id);

    $.get("php/get/bills_pay/index.php?id=" + id, function(data) {
      var json = JSON.parse(data)[0];

      $('#bills_pay_onoff').html(json.title);

      $('#modalDesativar').modal("show");
    });
  }
</script>