<?php
include_once("../php/conn/index.php");

$sql = "SELECT * FROM products";
$res_products = mysqli_query($conn, $sql);

$sql = "SELECT * FROM equipment";
$res_equipment = mysqli_query($conn, $sql);

$sql = "SELECT SUM(payment_value) FROM bills_pay WHERE bills_type = 'infraestrutura'";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
    $infra_cost = $row[0];
}
// regra de negocio
$infra_cost /= 168;


$sql = "SELECT COUNT(id) AS num_emp, SUM(salary_amount) AS total_salary FROM employees";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
    $num_emp = $row[0];
    $total_salary = $row[1];
}

// regra de negocio
$hours_disp = $num_emp * 168;
$hours_cost = $total_salary / $hours_disp;

$sql = "SELECT * FROM services";
$res_services = mysqli_query($conn, $sql);
?>
<div class="container-fluid">
    <div class="card shadow mb-4" style="height: 100%;">
        <div class="card-header py-3" style="position: relative; display:flex">
            <h6 class="m-0 font-weight-bold text-primary">Consultar Servicos</h6>
            <div class="nav-search-btn">
                <button class="btn btn-primary" style="width: 100%;margin-right: 10px;border-radius: 25px;" data-toggle="modal" data-target="#cadastrarModal">
                    <i class="fas fa-plus"></i>
                    <span>Cadastrar Servico</span>
                </button>
            </div>
        </div>
        <div class="card-body">
            <div id="accordion" style="height:100%; overflow-y:scroll;width:100%;padding-right: 10px;">
                <table class="table" id="tableServico">
                    <thead>
                        <tr>
                            <th scope="col">Nome do Serviço</th>
                            <th scope="col">Custo Total</th>
                            <th scope="col">Valor de Venda</th>
                            <th scope="col">Lucro</th>
                            <th scope="col">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php while ($row = mysqli_fetch_array($res_services)) { ?>
                            <tr>
                                <td><?= $row['name'] ?></td>
                                <td>R$<?= number_format($row['total_cost'], 2, ',', '.') ?></td>
                                <td>R$<?= number_format($row['sale_value'], 2, ',', '.') ?></td>
                                <td>R$<?= number_format($row['profit_value'], 2, ',', '.') ?></td>
                                <td>
                                    <button onclick="edit(<?= $row['id'] ?>)" class="btn btn-primary" style="border-radius: 25px;padding: 6px 9px;">
                                        <i class="far fa-eye"></i>
                                    </button>
                                    <?php if ($row['status'] == 1) { ?>
                                        <button type="button" onclick="on_off(<?= $row['id'] ?>,0)" class="btn btn-danger" style="border-radius: 25px;padding: 6px 12px;">
                                            <i class="fas fa-power-off"></i>
                                        </button>
                                    <?php } else if ($row['status'] == 0) { ?>
                                        <button type="button" onclick="on_off(<?= $row['id'] ?>,1)" class="btn btn-primary" style="border-radius: 25px;padding: 6px 12px;">
                                            <i class="fas fa-power-off"></i>
                                        </button>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="cadastrarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="padding:20px">
            <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h1>Cadastrar Servicos</h1>
            <form action="php/insert/services/" method="POST">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="name">Nome do Serviço</label>
                        <input type="text" id="name" name="name" class="form-control" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="description">Descrição</label>
                        <textarea rows="3" type="text" id="description" name="description" class="form-control" required></textarea>
                    </div>
                </div>
                <input type="hidden" id="qtd_products" name="qtd_products" value="1">
                <input type="hidden" id="qtd_equipment" name="qtd_equipment" value="1">

                <div id="div-products">
                    <div id="div-product-1" class="divs-more">
                        <div class="form-row">
                            <div class="col-md-4">
                                <label for="product_1">Produto</label>
                            </div>
                            <div class="col-md-2">
                                <label for="qtd_product_1">Quantidade</label>
                            </div>
                            <div class="col-md-2">
                                <label for="type_product_1">Tipo</label>
                            </div>
                            <div class="col-md-2">
                                <label for="pvalue_1">Valor Produto</label>
                            </div>
                            <div class="col-md-2">
                                <label>Mais</label>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <select onchange="seleciona_produto(this)" id="product_1" name="product_1" class="form-control" required>
                                    <option value="">Selecione o produto</option>
                                    <?php while ($row = mysqli_fetch_array($res_products)) { ?>
                                        <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <input onchange="atualiza_custos()" type="number" step="0.01" id="qtd_product_1" name="qtd_product_1" class="form-control" required>
                            </div>
                            <div class="form-group col-md-2">
                                <input type="text" id="type_product_1" name="type_product_1" class="form-control" readonly>
                            </div>
                            <div class="form-group col-md-2">
                                <input type="text" id="pvalue_1" name="pvalue_1" class="form-control" readonly>
                            </div>
                            <div class="form-group col-md-2">
                                <div class="btn btn-primary" onclick="novo_produto()">Adicionar</div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div id="div-equipment">
                    <div id="div-equipment-1" class="divs-more">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="equipment_1">Equipamento</label>
                            </div>
                            <div class="col-md-4">
                                <label for="evalue_1">Valor Equipamento</label>
                            </div>
                            <div class="col-md-2">
                                <label>Mais</label>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <select onchange="seleciona_equipamento(this)" id="equipment_1" name="equipment_1" class="form-control" required>
                                    <option value="">Selecione o equipamento</option>
                                    <?php while ($row = mysqli_fetch_array($res_equipment)) { ?>
                                        <option value="<?= $row['id'] ?>"><?= $row['model'] . ' - ' . $row['brand'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <input type="number" id="evalue_1" name="evalue_1" class="form-control" readonly>
                            </div>
                            <div class="form-group col-md-2">
                                <div class="btn btn-primary" onclick="novo_equipamento()">Adicionar</div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="hours_qtd">Qtd Horas</label>
                        <input min="1" onchange="atualiza_custos()" type="number" step="1" id="hours_qtd" name="hours_qtd" class="form-control" value="1" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="hours_cost">Valor Hora</label>
                        <input type="number" step="0.01" id="hours_cost" name="hours_cost" class="form-control" readonly>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="products_cost">Custo Produtos</label>
                        <input onchange="atualiza_custos()" type="number" id="products_cost" name="products_cost" class="form-control" readonly>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="equipment_cost">Custo Equipamentos</label>
                        <input onchange="atualiza_custos()" type="number" step="0.01" id="equipment_cost" name="equipment_cost" class="form-control" readonly>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="infra_cost">Custo Infraestrutura</label>
                        <input type="number" step="0.01" id="infra_cost" name="infra_cost" class="form-control" readonly>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="total_cost">Custo Total</label>
                        <input type="number" id="total_cost" name="total_cost" class="form-control" readonly>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="sale_value">Valor Venda</label>
                        <input onchange="atualiza_custos()" step="0.01" type="number" id="sale_value" name="sale_value" class="form-control" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="profit_value">Valor Lucro</label>
                        <input type="number" id="profit_value" name="profit_value" class="form-control" readonly>
                    </div>
                </div>

                <div style="text-align-last: center;">
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                </div>

            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="editaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="padding:20px">
            <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h1 style="color:#01B93C">Editar Serviço</h1>
            <form action="php/update/services/edit.php" method="POST">
                <div id="div-conteudo-edit">

                </div>
                <div style="text-align-last: center;">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal" aria-label="Close">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Editar</button>
                </div>
            </form>
        </div>
    </div>
</div>



<!-- DESATIVAR -->
<div class="modal fade" id="modalDesativar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" style="color: #01B93C;margin-left: 5px;" id="exampleModalLabel">Alerta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="php/update/services/onoff.php" method="POST">
                <input type="hidden" id="service_id" name="service_id">
                <input type="hidden" id="service_status" name="service_status">
                <div class="modal-body">
                    Tem certeza que deseja alterar o status do serviço <span style="color:#01B93C" id="service_onoff"></span><span id="status_nome"></span>?<br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Alterar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    var hours_cost_val = parseFloat(<?= $hours_cost ?>);
    var infra_cost_val = parseFloat(<?= $infra_cost ?>);

    $(document).ready(function() {
        $('#tableServico').DataTable();

        $('#hours_cost').val(hours_cost_val.toFixed(2));
        $('#infra_cost').val(infra_cost_val.toFixed(2));

        var hours_infra_cost = parseFloat($('#hours_cost').val()) + parseFloat($('#infra_cost').val());
        $('#total_cost').val(hours_infra_cost.toFixed(2));
    });

    var qtd_produtos = 1;
    var qtd_equipamentos = 1;
    var soma_custo_produtos = 0;
    var soma_custo_equipamentos = 0;

    function novo_produto(edit = '', ename = '') {


        var prox_id = parseInt($('#div-products' + edit + ' .divs-more').last()[0].id.split("-")[2]) + 1;

        $.get('php/get/utils/new_product.php?id_next=' + prox_id + "&edit=" + edit + "&ename=" + ename, function(data) {
            var div_insert = document.createElement("div");
            div_insert.setAttribute('class', 'divs-more');
            div_insert.setAttribute('id', 'div-product-' + prox_id + edit);

            div_insert.innerHTML = data;

            document.getElementById('div-products' + edit).appendChild(div_insert);

            if (ename != "") {
                qtd_produtos_edit++;
                $('#qtd_products_edit').val(qtd_produtos_edit);
            } else {
                qtd_produtos++;
                $('#qtd_products').val(qtd_produtos);
            }

            atualiza_custos(edit, ename);
        });
    }

    function remover_produto(id, edit = '', ename = '') {
        var div_remover = document.getElementById('div-product-' + id + edit);
        div_remover.parentNode.removeChild(div_remover);

        if (ename != "") {
            qtd_produtos_edit--;
            $('#qtd_products_edit').val(qtd_produtos_edit);
        } else {
            qtd_produtos--;
            $('#qtd_products').val(qtd_produtos);
        }

        atualiza_custos(edit, ename);
    }

    function novo_equipamento(edit = '', ename = '') {


        var prox_id = parseInt($('#div-equipment' + edit + ' .divs-more').last()[0].id.split("-")[2]) + 1;

        $.get('php/get/utils/new_equipment.php?id_next=' + prox_id + "&edit=" + edit + "&ename=" + ename, function(data) {
            var div_insert = document.createElement("div");
            div_insert.setAttribute('class', 'divs-more');
            div_insert.setAttribute('id', 'div-equipment-' + prox_id + edit);

            div_insert.innerHTML = data;

            document.getElementById('div-equipment' + edit).appendChild(div_insert);
            if (ename != "") {
                qtd_equipamentos_edit++;
                $('#qtd_equipment_edit').val(qtd_equipamentos_edit);
            } else {
                qtd_equipamentos++;
                $('#qtd_equipment').val(qtd_equipamentos);
            }
            atualiza_custos(edit, ename);
        });
    }

    function remover_equipamento(id, edit = '', ename = '') {
        var div_remover = document.getElementById('div-equipment-' + id + edit);
        div_remover.parentNode.removeChild(div_remover);

        if (ename != "") {
            qtd_equipamentos_edit--;
            $('#qtd_equipment_edit').val(qtd_equipamentos_edit);
        } else {
            qtd_equipamentos--;
            $('#qtd_equipment').val(qtd_equipamentos);
        }
        atualiza_custos(edit, ename);
    }

    function seleciona_produto(obj, edit = '', ename = '') {
        var i = obj.id.split("_")[1];

        if (obj.value != "") {
            $.get("php/get/products?id=" + obj.value, function(data) {
                var json = JSON.parse(data)[0];

                $('#type_product_' + i + ename).val(json.measurement);
                $('#pvalue_' + i + ename).val(parseFloat(json.cost_value) / parseFloat(json.quantity));

                atualiza_custos(edit, ename);
            });
        } else {
            $('#type_product_' + i + ename).val("");
            $('#pvalue_' + i + ename).val("");
        }
    }

    function seleciona_equipamento(obj, edit = '', ename = '') {

        var i = obj.id.split("_")[1];

        if (obj.value != "") {
            $.get("php/get/equipment?id=" + obj.value, function(data) {
                var json = JSON.parse(data)[0];

                $('#evalue_' + i + ename).val(json.cost_value);

                atualiza_custos(edit, ename);
            });
        } else {
            $('#evalue_' + i + ename).val("");
        }
    }

    function atualiza_custos(edit = '', ename = '') {
        // CALCULO DE CUSTO PRODUTOS
        soma_custo_produtos = 0;
        if (ename != "") {
            var qtd_produtos_aux = qtd_produtos_edit;
        } else {
            var qtd_produtos_aux = qtd_produtos;
        }

        for (var i = 1; i <= qtd_produtos_aux; i++) {
            // console.log(document.getElementById('qtd_product_' + i + ename));
            if (document.getElementById('qtd_product_' + i + ename)) {
                if ($('#qtd_product_' + i + ename).val() != "") {
                    // console.log($('#qtd_product_' + i + ename).val());
                    // console.log($('#pvalue_' + i + ename).val());
                    soma_custo_produtos += parseFloat($('#qtd_product_' + i + ename).val()) * parseFloat($('#pvalue_' + i + ename).val());
                    // console.log(soma_custo_produtos);
                }
            } else {
                qtd_produtos_aux++;
            }
        }
        // FIM CALCULO DE CUSTO PRODUTOS

        // CALCULO DE CUSTO EQUIPAMENTOS
        soma_custo_equipamentos = 0;

        if (ename != "") {
            var qtd_equipamentos_aux = qtd_equipamentos_edit;
        } else {
            var qtd_equipamentos_aux = qtd_equipamentos;
        }

        for (var i = 1; i <= qtd_equipamentos_aux; i++) {
            if (document.getElementById('evalue_' + i + ename)) {
                // console.log(document.getElementById('evalue_' + i + ename));
                soma_custo_equipamentos += parseFloat($('#evalue_' + i + ename).val());
            } else {
                qtd_equipamentos_aux++;
            }
        }

        // regra de negócio
        soma_custo_equipamentos /= 168;
        // FIM CALCULO DE CUSTO EQUIPAMENTOS

        // CALCULO DE CUSTOS GERAIS E ATRIBUIÇÃO NOS INPUTS
        if ($('#hours_qtd' + ename).val() != "") {

            $('#products_cost' + ename).val((parseFloat($('#hours_qtd' + ename).val()) * soma_custo_produtos).toFixed(2));
            $('#equipment_cost' + ename).val((parseFloat($('#hours_qtd' + ename).val()) * soma_custo_equipamentos).toFixed(2));
            $('#hours_cost' + ename).val((parseFloat($('#hours_qtd' + ename).val()) * parseFloat(hours_cost_val)).toFixed(2));
            $('#infra_cost' + ename).val((parseFloat($('#hours_qtd' + ename).val()) * parseFloat(infra_cost_val)).toFixed(2));

            var total_cost = parseFloat($('#products_cost' + ename).val()) + parseFloat($('#equipment_cost' + ename).val()) +
                parseFloat($('#hours_cost' + ename).val()) + parseFloat($('#infra_cost' + ename).val());
            $('#total_cost' + ename).val(total_cost.toFixed(2));
        } else {
            $('#products_cost' + ename).val(0);
            $('#equipment_cost' + ename).val(0);
            $('#hours_cost' + ename).val(0);
            $('#infra_cost' + ename).val(0);
            $('#total_cost' + ename).val(0);
        }
        // FIM CALCULO DE CUSTOS GERAIS E ATRIBUIÇÃO NOS INPUTS

        // CALCULO E ATRIBUIÇÃO DE CUSTO TOTAL
        var custo_total = parseFloat($('#products_cost' + ename).val()) + parseFloat($('#equipment_cost' + ename).val()) +
            parseFloat($('#infra_cost' + ename).val()) + parseFloat($('#hours_cost' + ename).val());

        $('#total_cost').val(custo_total.toFixed(2));
        // FIM CALCULO E ATRIBUIÇÃO DE CUSTO TOTAL

        // CALCULO E ATRIBUIÇÃO DE LUCRO
        if ($('#total_cost' + ename).val() != 0 && $('#total_cost' + ename).val() != "" && $('#sale_value' + ename).val() != "") {
            $('#profit_value' + ename).val((parseFloat($('#sale_value' + ename).val()) - parseFloat($('#total_cost' + ename).val())).toFixed(2));
        };
        // FIM CALCULO E ATRIBUIÇÃO DE LUCRO
    }

    function on_off(id, status) {
        $('#service_id').val(id);
        $('#service_status').val(status);

        var nome_stt = status == 0 ? " para inativo" : " para ativo";
        $('#status_nome').html(nome_stt);

        $.get("php/get/services/index.php?id=" + id, function(data) {
            var json = JSON.parse(data)[0];

            $('#service_onoff').html(json.name);

            $('#modalDesativar').modal("show");
        });
    }

    function edit(id) {
        $.get("php/get/utils/get_servico.php?id=" + id, function(data) {
            $('#div-conteudo-edit').html(data);

            $('#editaModal').modal("show");
        });
    }
</script>