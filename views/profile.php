<?php
session_start();
include_once("../php/conn/index.php");

$employee_id = $_SESSION['ZWxldHJpY2Ft'];

$sql = "SELECT * FROM employees WHERE id = $employee_id";
$res = mysqli_query($conn, $sql);

while ($row = mysqli_fetch_array($res)) {
    $login = $row['login'];
    $password = $row['password'];
    $name = $row['name'];
    $document = $row['document'];
    $salary_amount = $row['salary_amount'];
    $role = $row['role'];
    $zip = $row['zip'];
    $street = $row['street'];
    $number = $row['number'];
    $neighborhood = $row['neighborhood'];
    $city = $row['city'];
    $state = $row['state'];
    $complement = $row['complement'];
}

$sql = "SELECT * FROM permissions";
$res_permissions = mysqli_query($conn, $sql);

$sql = "SELECT * FROM employees_permissions WHERE employee_id = $employee_id";
$res_employee_permissions = mysqli_query($conn, $sql);
$count_permisssions = mysqli_num_rows($res_employee_permissions);

$cont = 0;
$permissions_ids = "";
while ($row_p = mysqli_fetch_array($res_employee_permissions)) {
    $cont += 1;
    // $permissions_ids .= "'";
    $permissions_ids .= $row_p['permission_id'];
    // $permissions_ids .= "'";

    if ($cont != $count_permisssions) {
        $permissions_ids .= ",";
    }
}
?>
<div class="container-fluid">
    <div class="card shadow mb-4" style="height: 100%;">
        <div class="card-header py-3" style="position: relative;text-align:center">
            <h6 class="m-0 font-weight-bold text-primary">Meus Dados</h6>

        </div>
        <div class="card-body">
            <form action="php/update/employees/edit.php" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="employee_id_edit" id="employee_id_edit" value="<?= $employee_id ?>">
                <input type="hidden" name="form_type" id="form_type" value="profile">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="name_edit">Nome</label>
                        <input type="text" id="name_edit" name="name_edit" class="form-control" placeholder="Nome" value="<?= $name ?>" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="document_edit">CPF</label>
                        <input type="text" id="document_edit" name="document_edit" class="form-control" placeholder="999.999.999-99" value="<?= $document ?>" required>
                        <input type="hidden" id="document_edit_old" name="document_edit_old">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="salary_edit">Salário</label>
                        <input type="number" step="0.01" id="salary_edit" name="salary_edit" class="form-control" placeholder="999,99" value="<?= $salary_amount ?>" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="role_edit">Cargo</label>
                        <input type="text" id="role_edit" name="role_edit" class="form-control" placeholder="Cargo" value="<?= $role ?>" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="login_edit">Login</label>
                        <input onchange="verificar_login(this)" type="text" id="login_edit" name="login_edit" class="form-control" placeholder="Login" value="<?= $login ?>" required>
                        <input type="hidden" id="login_edit_old" name="login_edit_old">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="password_edit">Senha</label>
                        <input type="password" id="password_edit" name="password_edit" class="form-control" placeholder="Senha" value="<?= $password ?>" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="zip_edit">CEP</label>
                        <input onchange="busca_cep(this)" type="text" id="zip_edit" name="zip_edit" class="form-control" placeholder="99999-999" value="<?= $zip ?>" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="stret_edit">Endereço</label>
                        <input type="text" id="street_edit" name="street_edit" class="form-control" placeholder="Endereço" value="<?= $street ?>" required>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="number_edit">Número</label>
                        <input type="number" step="1" id="number_edit" name="number_edit" class="form-control" placeholder="999" value="<?= $number ?>" required>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="neighborhood_edit">Bairro</label>
                        <input type="text" id="neighborhood_edit" name="neighborhood_edit" class="form-control" placeholder="Bairro" value="<?= $neighborhood ?>" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="city_edit">Cidade</label>
                        <input type="text" id="city_edit" name="city_edit" class="form-control" placeholder="Cidade" value="<?= $city ?>" required>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="state_edit">UF</label>
                        <select id="state_edit" name="state_edit" class="form-control" placehodlder="UF" value="<?= $state ?>" required>
                            <option value="AC">AC</option>
                            <option value="AL">AL</option>
                            <option value="AP">AP</option>
                            <option value="AM">AM</option>
                            <option value="BA">BA</option>
                            <option value="CE">CE</option>
                            <option value="DF">DF</option>
                            <option value="ES">ES</option>
                            <option value="GO">GO</option>
                            <option value="MA">MA</option>
                            <option value="MT">MT</option>
                            <option value="MS">MS</option>
                            <option value="MG">MG</option>
                            <option value="PA">PA</option>
                            <option value="PB">PB</option>
                            <option value="PR">PR</option>
                            <option value="PE">PE</option>
                            <option value="PI">PI</option>
                            <option value="RJ">RJ</option>
                            <option value="RN">RN</option>
                            <option value="RS">RS</option>
                            <option value="RO">RO</option>
                            <option value="RR">RR</option>
                            <option value="SC">SC</option>
                            <option value="SP">SP</option>
                            <option value="SE">SE</option>
                            <option value="TO">TO</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="complement_edit">Complemento</label>
                        <input type="text" id="complement_edit" name="complement_edit" class="form-control" placeholder="Complemento" value="<?= $complement ?>">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="permissions_edit">Permissões</label>
                        <select placeholder="Clique e insira as permissões" id="permissions_edit" name="permissions_edit[]" multiple="multiple" required>
                            <?php while ($row = mysqli_fetch_array($res_permissions)) { ?>
                                <option value="<?= $row['id'] ?>"><?= $row['permission'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div style="text-align-last: center;">
                    <button type="submit" class="btn btn-primary">
                        <i class="fas fa-edit"></i>
                        Editar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#permissions_edit').selectize({
            plugins: ["remove_button"],
            delimiter: ',',
            persist: false
        });

        var permissoes = '<?= $permissions_ids ?>';
        permissoes = permissoes.split(',');
        console.log(permissoes);

        var selectize_permissoes = document.getElementById("permissions_edit");
        selectize_permissoes.selectize.setValue(permissoes);
    });
</script>