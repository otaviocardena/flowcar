<?php
include_once("../php/conn/index.php");

$sql = "SELECT * FROM services WHERE status = 1";
$res_services = mysqli_query($conn, $sql);

$sql = "SELECT id, responsible_name AS Nome, total_value AS Total, status FROM sales";
$res_sales = mysqli_query($conn, $sql);

$sql = "SELECT * FROM clients WHERE status = 1";
$res_clients = mysqli_query($conn, $sql);
?>
<div class="container-fluid">
    <div class="card shadow mb-4" style="height: 100%;">
        <div class="card-header py-3" style="position: relative; display:flex">
            <h6 class="m-0 font-weight-bold text-primary">Consultar Orcamento</h6>
            <div class="nav-search-btn">
                <button class="btn btn-primary" style="width: 100%;margin-right: 10px;border-radius: 25px;" data-toggle="modal" data-target="#cadastrarModal">
                    <i class="fas fa-plus"></i>
                    <span>Cadastrar Orcamento</span>
                </button>
            </div>
        </div>
        <div class="card-body">
            <div id="accordion" style="height:100%; overflow-y:scroll;width:100%;padding-right: 10px;">
                <table class="table" id="tableOrcamento">
                    <thead>
                        <tr>
                            <th scope="col">Nome do Cliente</th>
                            <th scope="col">Valor Total</th>
                            <th scope="col">Status</th>
                            <th scope="col">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php while ($row = mysqli_fetch_array($res_sales)) {

                            if ($row['status'] == 0) {
                                $status = "Orçamento Aberto";
                            } else if ($row['status'] == 1) {
                                $status = "Agendado";
                            } else {
                                $status = "Cancelado";
                            }
                        ?>
                            <tr>
                                <td><?= $row['Nome'] ?></td>
                                <td><?= $row['Total'] ?></td>
                                <td><?= $status ?></td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Ações
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <?php if ($row['status'] == 0 || $row['status'] == 2) { ?>
                                                <a onclick="edit(<?= $row['id'] ?>)" class="dropdown-item" data-toggle="modal" data-target="#editaModal" style="cursor:pointer"><i class="fas fa-eye" style="margin-right:5px;"></i>Editar</a>

                                                <a onclick="verifica_cliente(<?= $row['id'] ?>)" class="dropdown-item" style="cursor:pointer"><i class="fas fa-calculator" style="margin-right:5px;"></i>Agendar</a>

                                                <?php if ($row['status'] == 0) { ?>
                                                    <a onclick="on_off(<?= $row['id'] ?>,2)" class="dropdown-item" data-toggle="modal" data-target="#modalDesativar" style="cursor:pointer"><i class="fas fa-power-off" style="margin-right:5px"></i>Desativar</a>
                                                <?php } else if ($row['status'] == 2) { ?>
                                                    <a onclick="on_off(<?= $row['id'] ?>,0)" class="dropdown-item" data-toggle="modal" data-target="#modalDesativar" style="cursor:pointer"><i class="fas fa-power-off" style="margin-right:5px"></i>Ativar</a>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <a href="#" onclick="page('agenda.php')" class="dropdown-item"><i class="fas fa-calculator" style="margin-right:5px;"></i>Acessar Agendamentos</a>
                                            <?php } ?>

                                            <a onclick="imprimir(<?= $row['id'] ?>)" class="dropdown-item" target="_blank" style="cursor:pointer" href="php/get/utils/get_impressao.php?id=<?= $row['id'] ?>"><i class="fas fa-print" style="margin-right:5px"></i>Imprimir</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="cadastrarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="padding:20px">
            <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h1>Cadastrar Orcamento</h1>
            <form action="php/insert/sales/" method="POST" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="document_type">Tipo de Cadastro</label>
                        <select onchange="seleciona_tipo(this)" id="document_type" name="document_type" class="form-control">
                            <option value="cpf">CPF</option>
                            <option value="cnpj">CNPJ</option>
                        </select>
                    </div>
                    <input type="hidden" id="qtd_services" name="qtd_services" value="1">
                    <div class="form-group col-md-3 show" id="div_cpf">
                        <label for="cpf">CPF</label>
                        <input onchange="verificar_documento(this)" id="cpf" name="cpf" type="text" class="form-control">
                    </div>
                    <div class="form-group col-md-3 hide" id="div_cnpj">
                        <label for="cnpj">CNPJ</label>
                        <input onchange="verificar_documento(this)" id="cnpj" name="cnpj" type="text" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="client_name">Nome do Cliente</label>
                        <input id="client_name" name="client_name" type="text" class="form-control" required>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="license_plate">Placa Veículo</label>
                        <input onchange="validaPlaca(this)" onkeydown="upperCase(this)" id="license_plate" name="license_plate" type="text" class="form-control" maxlength="7" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-5">
                        <label for="services">Serviço</label>
                    </div>
                    <div class="col-md-5">
                        <label for="total_value">Valor Serviço</label>
                    </div>
                    <div class="col-md-2">
                        <label>Mais</label>
                    </div>
                </div>
                <div id="div-services">
                    <div id="div-service-1" class="divs-more">
                        <div class="form-row">
                            <div class="form-group col-md-5">
                                <select onchange="seleciona_services(this)" name="service_1" id="service_1" class="form-control" required>
                                    <option value="">Selecione um</option>
                                    <?php while ($row = mysqli_fetch_array($res_services)) { ?>
                                        <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group col-md-5">
                                <input id="service_value_1" name="service_value_1" type="number" class="form-control" min="0" readonly>
                            </div>
                            <div class="form-group col-md-2">
                                <div class="btn btn-primary" onclick="novo_servico()">Adicionar</div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="form-row">
                    <div class="form-group col -md-4">
                        <label for="payment_date">Dt Pagamento</label>
                        <input id="payment_date" name="payment_date" type="date" class="form-control" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="total_value">Valor Total</label>
                        <input id="total_value" name="total_value" type="number" class="form-control" min="0" readonly>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="discount">Desconto</label>
                        <input onchange="subtrairDesconto()" id="discount" name="discount" type="number" class="form-control" min="0" required>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="sale_obs">Observação</label>
                        <textarea id="sale_obs" name="sale_obs" rows="3" class="form-control" required></textarea>
                    </div>
                </div>

                <div style="text-align-last: center;">
                    <button type="submit" class="btn btn-primary">Cadastrar Orcamento</button>
                </div>

            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="editaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="padding:20px">
            <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h1 style="color:#01B93C">Editar Orçamento</h1>
            <form action="php/update/sales/edit.php" method="POST">
                <div id="content-edit">

                </div>
                <div style="text-align-last: center;">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal" aria-label="Close">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Editar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalAgendamento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="padding:20px">
            <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h1 style="color:#01B93C">Agendar Orçamento</h1>
            <form action="php/update/sales/schedule.php" method="POST">
                <input type="hidden" id="sale_id_to_schedule" name="sale_id_to_schedule">

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="schedule_time">Data Agendamento</label>
                        <input onchange="troca_datapicker(this)" type="date" id="schedule_date" name="schedule_date" class="form-control" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="schedule_time">Horario</label>
                        <input type="time" id="schedule_time" name="schedule_time" class="form-control" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12" style="text-align: -webkit-center;">
                        <div id="calendar"></div>
                    </div>
                </div>
                <hr>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div id="div-orc-view">

                        </div>
                    </div>
                </div>

                <div style="text-align-last: center;">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal" aria-label="Close">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Confirmar Agendamento</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- DESATIVAR -->
<div class="modal fade" id="modalDesativar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" style="color: #01B93C;margin-left: 5px;" id="exampleModalLabel">Alerta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="php/update/sales/onoff.php" method="POST">
                <input type="hidden" id="sale_id" name="sale_id">
                <input type="hidden" id="sale_status" name="sale_status">
                <div class="modal-body">
                    Tem certeza que deseja alterar o status do orçamento do responsável <span style="color:#01B93C" id="sale_onoff"></span><span id="status_nome"></span>?<br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Alterar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalVerificaCad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" style="color: #01B93C;margin-left: 5px;" id="exampleModalLabel">Alerta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                O cliente <span id="name_client" style="color:#01B93C"></span> já está cadastrado?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="cad_cliente()">Não</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" data-toggle="modal" data-target="#modalSelectCli">Sim</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalSelectCli" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" style="color: #01B93C;margin-left: 5px;" id="exampleModalLabel">Alerta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="sale_id_to_client" name="sale_id_to_client">
                <label>Cliente</label>
                <select name="sale_client_id" id="sale_client_id" class="form-control" required>
                    <option value="">Selecione o cliente</option>
                    <?php while ($row = mysqli_fetch_array($res_clients)) { ?>
                        <option value="<?= $row['id'] ?>"><?= $row['fantasy_name'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button onclick="selecionar_cliente()" type="button" class="btn btn-primary">Selecionar</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#tableOrcamento').DataTable();
        $('#cpf').mask('999.999.999-99');
        $('#cpf_edit').mask('999.999.999-99');
        $('#cnpj').mask('99.999.999/9999-99');
        $('#cnpj_edit').mask('99.999.999/9999-99');

        $('#calendar').datepicker({
            defaultDate: new Date(),
            inline: true,
            dateFormat: 'yy-mm-dd',
            firstDay: 1,
            showOtherMonths: true,
            dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            monthNames: [
                'Janeiro',
                'Fevereiro',
                'Março',
                'Abril',
                'Maio',
                'Junho',
                'Julho',
                'Agosto',
                'Setembro',
                'Outubro',
                'Novembro',
                'Dezembro'
            ],
            monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
            onSelect: function(dateText) {
                $('#schedule_date').val(dateText);
            }
        }, );
        $('#schedule_date').val('<?= date('Y-m-d') ?>');
    });

    var qtd_services = 1
    var soma_custo_servicos = 0;

    function seleciona_tipo(obj) {
        var complemento = obj.id.split("_");
        if (complemento.length > 2) {
            complemento = "_" + complemento[2];
        } else {
            complemento = "";
        }

        $('#div_cnpj' + complemento).removeClass("show").addClass("hide");
        $('#div_cpf' + complemento).removeClass("show").addClass("hide");

        $('#cnpj' + complemento).val("");
        $('#cpf' + complemento).val("");

        if (obj.value == "cnpj") {
            document.getElementById('cnpj' + complemento).setAttribute("required", true);
            document.getElementById('cpf' + complemento).removeAttribute("required");
        } else {
            document.getElementById('cpf' + complemento).setAttribute("required", true);
            document.getElementById('cnpj' + complemento).removeAttribute("required");
        }

        $('#div_' + obj.value + complemento).removeClass("hide").addClass("show");
        // console.log($('#div_' + obj.value + complemento));
    }

    function novo_servico(edit = '', ename = '') {
        var prox_id = parseInt($('#div-services' + edit + ' .divs-more').last()[0].id.split("-")[2]) + 1;


        $.get('php/get/utils/new_service.php?id_next=' + prox_id + "&edit=" + edit + "&ename=" + ename, function(data) {
            var div_insert = document.createElement("div");
            div_insert.setAttribute('class', 'divs-more');
            div_insert.setAttribute('id', 'div-service-' + prox_id + edit);

            div_insert.innerHTML = data;

            document.getElementById('div-services' + edit).appendChild(div_insert);
            if (ename != "") {
                qtd_services_edit++;
                $('#qtd_services_edit').val(qtd_services_edit);
            } else {
                qtd_services++;
                $('#qtd_services').val(qtd_services);
            }

            altera_valor_servicos(ename);
        });

    }

    function remover_servico(id, edit = '', ename = '') {
        var div_remover = document.getElementById('div-service-' + id + edit);
        div_remover.parentNode.removeChild(div_remover);

        if (ename != "") {
            qtd_services_edit--;
            $('#qtd_services_edit').val(qtd_services_edit);
        } else {
            qtd_services--;
            $('#qtd_services').val(qtd_services);
        }
        altera_valor_servicos(ename);
    }

    function upperCase(obj) {
        setTimeout(function() {
            obj.value = obj.value.toUpperCase();
        }, 1);
    }

    function validaPlaca(obj) {
        var license_plate = obj.value;
        const regexPlaca_2 = /[A-Z]{3}[0-9][0-9A-Z][0-9]{2}/;
        var table = "sales";

        var complemento = obj.id.split("_");
        if (complemento.length > 2) {
            complemento = "_" + complemento[2];
            var license_plate_old = $('#license_plate_old_edit').val();
        } else {
            complemento = "";
        }


        if (license_plate != "") {
            if (!regexPlaca_2.test(license_plate)) {
                alert("Placa inválida");
                $('#license_plate' + complemento).val("");
            }
            $.get("php/get/utils/verify_license_plate.php?license_plate=" + license_plate + "&table=" + table, function(data) {
                if (parseInt(data) > 0) {
                    if (license_plate_old) {
                        // edit
                        if (license_plate != license_plate_old) {
                            $('#license_plate' + complemento).val("");
                            alert("Placa já cadastrada!");
                        }
                    } else {
                        // cadastro
                        $('#license_plate').val("");
                        alert("Placa já cadastrada!");
                    }
                }
            });
        }
    }

    function seleciona_services(obj, edit = '', ename = '') {
        var i = obj.id.split("_")[1];

        if (obj.value != "") {
            $.get("php/get/services?id=" + obj.value, function(data) {
                var json = JSON.parse(data)[0];

                $('#service_value_' + i + ename).val(parseFloat(json.sale_value));
                altera_valor_servicos(ename);
            });
        } else {
            $('#service_value_' + i + ename).val("");
        }
    }

    function altera_valor_servicos(ename = "") {
        soma_custo_servicos = 0;

        if (ename != "") {
            var qtd_services_aux = qtd_services_edit;
        } else {
            var qtd_services_aux = qtd_services;
        }

        for (var i = 1; i <= qtd_services_aux; i++) {
            if (document.getElementById('service_value_' + i + ename)) {
                var valor_soma = $('#service_value_' + i + ename).val();
                soma_custo_servicos += parseFloat(valor_soma);
                console.log(i);
                console.log(soma_custo_servicos);
            } else {
                qtd_services_aux++;
            }
        }


        if ($('#discount' + ename).val() != "") {
            var desconto = parseFloat($('#discount' + ename).val());
            soma_custo_servicos -= desconto;
            $("#total_value" + ename).val(soma_custo_servicos.toFixed(2));
        } else {
            $("#total_value" + ename).val(soma_custo_servicos.toFixed(2));
        }
    }

    function subtrairDesconto(ename = "") {
        if (ename != "") {
            var qtd = qtd_services_edit;
        } else {
            var qtd = qtd_services;
        }

        var desconto = parseFloat($('#discount' + ename).val());
        soma_custo_servicos = 0;

        for (var i = 1; i <= qtd; i++) {
            // console.log(document.getElementById('service_value_' + i + ename));
            if (document.getElementById('service_value_' + i + ename)) {
                var valor_soma = $('#service_value_' + i + ename).val();
                soma_custo_servicos += parseFloat(valor_soma);
            } else {
                qtd++;
            }
        }

        // var valor_total = parseFloat($('#total_value' + ename).val());
        soma_custo_servicos -= desconto;
        $("#total_value" + ename).val(soma_custo_servicos.toFixed(2));
    }


    function verificar_documento(obj) {
        var documento = obj.value;
        var tabela = "sales";

        var complemento = obj.id.split("_");
        var tipo = complemento[0];
        if (complemento.length > 1) {
            complemento = "_" + complemento[1];
            var documento_old = $('#document_edit_old').val();
        } else {
            complemento = "";
        }


        $.get("php/get/utils/verify_document.php?doc=" + documento + "&table=" + tabela, function(data) {
            if (parseInt(data) > 0) {
                if (documento_old) {
                    // edit
                    if (documento != documento_old) {
                        if (tipo == 'cpf' && obj.value != "") {
                            $('#cpf' + complemento).val("");
                            alert("CPF já cadastrado!");
                        } else if (obj.value != "") {
                            $('#CNPJ' + complemento).val("");
                            alert("CNPJ já cadastrado!");
                        }
                    }
                } else {
                    // cadastro
                    if (obj.id == 'cpf' && obj.value != "") {
                        $('#cpf' + complemento).val("");
                        alert("CPF já cadastrado!");
                    } else if (obj.value != "") {
                        $('#cnpj' + complemento).val("");
                        alert("CNPJ já cadastrado!");
                    }
                }
            }
        });
    }

    function edit(id) {
        $.get("php/get/utils/get_orcamento.php?id=" + id, function(data) {

            $('#content-edit').html(data);
            $('#editaModal').modal('show');
        });
    }


    function on_off(id, status) {
        $('#sale_id').val(id);
        $('#sale_status').val(status);

        var nome_stt = status == 2 ? " para inativo" : " para ativo";
        $('#status_nome').html(nome_stt);

        $.get("php/get/sales?id=" + id, function(data) {
            var json = JSON.parse(data)[0];

            $('#sale_onoff').html(json.responsible_name);

            $('#modalDesativar').modal("show");
        });
    }

    function verifica_cliente(id) {
        $.get("php/get/sales?id=" + id, function(data) {
            var json = JSON.parse(data)[0];

            if (json.client_id == null) {
                $('#sale_id_to_client').val(id);
                $('#name_client').html(json.responsible_name);

                $('#modalVerificaCad').modal("show");
            } else {
                $.get("php/get/utils/view_orcamento.php?id=" + id, function(view) {
                    $('#sale_id_to_schedule').val(id);
                    $('#div-orc-view').html(view);

                    $('#modalAgendamento').modal("show");
                });
            }
        });
    }

    function selecionar_cliente() {
        if ($('#sale_client_id').val() != "") {
            var sale_id = $('#sale_id_to_client').val();
            var client_id = $('#sale_client_id').val();

            $.post('php/update/sales/select_client.php', {
                sale_id,
                client_id
            }).done(function(data) {
                if (data == "OK") {
                    $.get("php/get/utils/view_orcamento.php?id=" + sale_id, function(view) {
                        $('#sale_id_to_schedule').val(sale_id);
                        $('#div-orc-view').html(view);

                        $('#modalSelectCli').modal("hide");
                        $('#modalAgendamento').modal("show");
                    });
                } else {
                    alert("Houve algum erro ao selecionar cliente.");
                }
            });
        } else {
            alert("Selecione um cliente primeiro.")
        }
    }

    function cad_cliente() {
        setTimeout(function() {
            page('cliente.php?modal=show');
        }, 300);
    }

    function troca_datapicker(obj) {
        $("#calendar").datepicker("setDate", obj.value);
    }
</script>