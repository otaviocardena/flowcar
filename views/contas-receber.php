<?php
include_once("../php/conn/index.php");

$sql = "SELECT 
            s.responsible_name, 
            b.status, 
            s.payment_date, 
            s.total_value,
            b.id,
            b.sale_id, 
            b.receive_date
        FROM bills_receive 
        AS b 
        LEFT JOIN sales 
        AS s 
        ON b.sale_id = s.id";

$res_receive = mysqli_query($conn, $sql);
$status = "";

?>

<div class="container-fluid">
    <div class="card shadow mb-4" style="height: 100%;">
        <div class="card-header py-3" style="position: relative; display:flex">
            <h6 class="m-0 font-weight-bold text-primary">Contas a Receber</h6>
        </div>
        <div class="card-body">
            <div id="accordion" style="height:100%; overflow-y:scroll;width:100%;padding-right: 10px;">
                <table class="table" id="tableReceber">
                    <thead>
                        <tr>
                            <th scope="col">Nome do Cliente</th>
                            <th scope="col">Status</th>
                            <th scope="col">Data Pagamento</th>
                            <th scope="col">Valor</th>
                            <th scope="col">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php while ($row = mysqli_fetch_array($res_receive)) {
                            if ($row['status'] == 0) {
                                $status = "Em Aberto";
                            } else if ($row['status'] == 1) {
                                $status = "Pago";
                            } else if ($row['status'] == 2) {
                                $status = "Cancelado/Inativo";
                            }
                        ?>
                            <tr>
                                <td><?= $row['responsible_name'] ?> </td>
                                <td><?= $status ?></td>
                                <td><?= date('d/m/Y', strtotime($row['payment_date'])) ?></td>
                                <td>R$&nbsp;<?= number_format($row['total_value'], 2, ',', '.') ?></td>
                                <td>
                                    <?php if ($row['status'] == 0) { ?>
                                        <button onclick="edit(<?= $row['sale_id'] ?>, <?= $row['id'] ?>)" class="btn btn-primary" style="border-radius: 25px;padding: 6px 9px;" data-toggle="modal" data-target="#editaModal">
                                            <i class="far fa-eye"></i>
                                        </button>
                                        <button onclick="on_off(<?= $row['sale_id'] ?>, <?= $row['status'] ?>, <?= $row['id'] ?>)" class="btn btn-danger" data-toggle="modal" data-target="#modalDesativar" style="border-radius: 25px;padding: 6px 10px;">
                                            <i class="fas fa-power-off"></i>
                                        </button>
                                    <?php } else if ($row['status'] == 1) { ?>
                                        <button onclick="visualiza(<?= $row['sale_id'] ?>, <?= $row['id'] ?>)" class="btn btn-primary" style="border-radius: 25px;padding: 6px 9px;" data-toggle="modal" data-target="#visualizaModal">
                                            <i class="far fa-eye"></i>
                                        </button>
                                    <?php } else { ?>
                                        <button onclick="visualiza(<?= $row['sale_id'] ?>, <?= $row['id'] ?>)" class="btn btn-primary" style="border-radius: 25px;padding: 6px 9px;" data-toggle="modal" data-target="#visualizaModal">
                                            <i class="far fa-eye"></i>
                                        </button>
                                        <button onclick="on_off(<?= $row['sale_id'] ?>, <?= $row['status'] ?>, <?= $row['id'] ?>)" class="btn btn-primary" data-toggle="modal" data-target="#modalDesativar" style="border-radius: 25px;padding: 6px 10px;">
                                            <i class="fas fa-power-off"></i>
                                        </button>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="cadastrarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="padding:20px">
            <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h1>Cadastrar conta a Receber</h1>
            <form>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="inputRazao">Nome do Serviço</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputRazao">Placa Veículo</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputRazao">Valor Final</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputRazao">Dt Pagamento</label>
                        <input type="date" class="form-control">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputRazao">Serviço</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputRazao">Valor Serviço</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputRazao">Serviço</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputRazao">Valor Serviço</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
                <hr>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="inputRazao">Dt Emissão</label>
                        <input type="date" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputRazao">dt Agenda</label>
                        <input type="date" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputRazao">Observação</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
                <div style="text-align-last: center;">
                    <button type="submit" class="btn btn-primary">Cadastrar Receber</button>
                </div>

            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="editaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="padding:20px">
            <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h1 style="color:#01B93C">Validar Conta a Receber </h1>
            <form action="php/update/bills_receive/edit.php" method="POST">
                <div id="content-edit">
                </div>
                <input type="hidden" id="bills_receive_id" name="bills_receive_id">
                <div style="text-align-last: center;">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal" aria-label="Close">Cancelar</button>
                    <button type="submit" class="btn btn-primary">
                        <i class="fas fa-edit"></i>
                        Receber</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="visualizaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="padding:20px">
            <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h1 style="color:#01B93C">Visualizar Conta a Receber </h1>
            <div id="content-visualiza">
            </div>
            <div style="text-align-last: center;">
                <button class="btn btn-secondary" type="button" data-dismiss="modal" aria-label="Close">Fechar</button>
            </div>
        </div>
    </div>
</div>


<!-- DESATIVAR -->
<div class="modal fade" id="modalDesativar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" style="color: #01B93C;margin-left: 5px;" id="exampleModalLabel">Alerta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="php/update/bills_receive/onoff.php" method="POST">
                <div class="modal-body">
                    <input type="hidden" id="bills_receive_id_onoff" name="bills_receive_id_onoff">
                    <input type="hidden" id="sale_id_onoff" name="sale_id_onoff">
                    <input type="hidden" id="bills_receive_status_onoff" name="bills_receive_status_onoff">
                    Tem certeza que deseja excluir Conta a Receber?<br>
                    <span id="bills_receive_text" style="color:#01B93C">Data pagamento - valor</span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Alterar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#tableReceber').DataTable();
    });

    function edit(id, receive_id) {
        $.get("php/get/utils/view_orcamento.php?id=" + id, function(data) {

            $('#content-edit').html(data);
            $('#editaModal').modal('show');
            $('#bills_receive_id').val(receive_id);
        });
    }

    function visualiza(id, receive_id) {
        $.get("php/get/utils/view_orcamento.php?id=" + id, function(data) {

            $('#content-visualiza').html(data);
            $('#visualizaModal').modal('show');
        });
    }

    function on_off(id, status, receive_id) {
        $('#sale_id_onoff').val(id);
        $('#bills_receive_id_onoff').val(receive_id);

        if (status == 0) {
            status = 2;
        } else {
            status = 0;
        }

        $('#bills_receive_status_onoff').val(status);

        var nome_stt = status == 0 ? "para inativo" : "para ativo";
        $('#status_nome').html(nome_stt);

        $.get("php/get/sales/index.php?id=" + id, function(data) {
            var json = JSON.parse(data)[0];

            $('#bills_receive_text').html(json.payment_date + " - " + "R$" + json.total_value);
            $('#modalDesativar').modal("show");
        });
    }
</script>