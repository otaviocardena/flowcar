<?php
include_once("../php/conn/index.php");
$sql = "SELECT id,fantasy_name FROM clients";
$res_owner = mysqli_query($conn, $sql);
$res_owner_edit = mysqli_query($conn, $sql);

$sql = "SELECT v.id,
            v.model,
            v.license_plate,
            v.status,
            c.fantasy_name AS 'owner' 
        FROM vehicles AS v 
        LEFT JOIN clients AS c ON 
            v.client_id = c.id";
$res_vehicles = mysqli_query($conn, $sql);
?>

<div class="container-fluid">
    <div class="card shadow mb-4" style="height: 100%;">
        <div class="card-header py-3" style="position: relative; display:flex">
            <h6 class="m-0 font-weight-bold text-primary">Automóveis</h6>
            <div class="nav-search-btn">
                <button class="btn btn-primary" style="width: 100%;margin-right: 10px;border-radius: 25px;" data-toggle="modal" data-target="#cadastrarModal">
                    <i class="fas fa-plus"></i>
                    <span>Cadastrar Automóvel</span>
                </button>
            </div>
        </div>
        <div class="card-body">
            <table class="table" id="DataTableClientes">
                <thead>
                    <tr>
                        <th scope="col">Modelo</th>
                        <th scope="col">Placa</th>
                        <th scope="col">Proprietário</th>
                        <th scope="col">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php while ($row = mysqli_fetch_array($res_vehicles)) { ?>
                        <tr>
                            <td><?= $row['model'] ?></td>
                            <td><?= $row['license_plate'] ?></td>
                            <td><?= $row['owner'] ?></td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Ações
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a onclick="edit(<?= $row['id'] ?>)" class="dropdown-item" data-toggle="modal" data-target="#editaModal" style="cursor:pointer"><i class="fas fa-eye" style="margin-right:5px;"></i>Visualizar/Editar</a>
                                        <a onclick="" class="dropdown-item" data-toggle="modal" data-target="#modalOrcamento" style="cursor:pointer"><i class="fas fa-calculator" style="margin-right:5px;"></i>Orçamento</a>
                                        <!-- <a class="dropdown-item" data-toggle="modal" data-target="#modalAtivar" style="cursor:pointer"><i class="fas fa-power-off" style="margin-right:5px"></i>Pagar</a> -->
                                        <?php if ($row['status'] == 1) { ?>
                                            <a onclick="on_off(<?= $row['id'] ?>,<?= $row['status'] ?>)" class="dropdown-item" data-toggle="modal" data-target="#modalDesativar" style="cursor:pointer"><i class="fas fa-power-off" style="margin-right:5px"></i>Desativar</a>
                                        <?php } else if ($row['status'] == 0) { ?>
                                            <a onclick="on_off(<?= $row['id'] ?>,<?= $row['status'] ?>)" class="dropdown-item" data-toggle="modal" data-target="#modalDesativar" style="cursor:pointer"><i class="fas fa-power-off" style="margin-right:5px"></i>Ativar</a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>



<div class="modal fade" id="cadastrarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="padding:20px">
            <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h1>Cadastrar novo automóvel</h1>
            <form action="php/insert/vehicles/index.php" method="POST" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label>Modelo</label>
                        <input id="model" name="model" type="text" placeholder="Modelo" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label>Marca</label>
                        <input id="brand" name="brand" type="text" placeholder="Marca" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label>Cor</label>
                        <input id="color" name="color" type="text" placeholder="Cor" class="form-control">
                    </div>
                    <div class="form-group col-md-2">
                        <label>Ano</label>
                        <input id="car_year" name="car_year" type="text" placeholder="9999" class="form-control">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label>Ano Modelo</label>
                        <input id="model_year" name="model_year" type="text" placeholder="9999" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label>Placa</label>
                        <input onkeydown="upperCase(this)" onchange="validaPlaca(this)" id="license_plate" name="license_plate" type="text" placeholder="Placa" class="form-control" maxlength="7">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="owner">Proprietário</label>
                        <select id="owner" name="owner" placeholder="Proprietário" class="form-control" required>
                            <option value="">Selecione um</option>
                            <?php while ($row = mysqli_fetch_array($res_owner)) { ?>
                                <option value="<?= $row['id'] ?>"><?= $row['fantasy_name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <input onchange="photo_insert(this)" type="file" id="vehicle_photo" name="vehicle_photo" style="display:none;" accept=".jpeg,.jpg,.png">
                        <a onclick="input_photo(this)" id="false_input" class="btn btn-light btn-icon-split" style="position:relative; margin: 4px 0px;background: #2D2734;border: none;">
                            <div id="icon_1" class="icon-grey">
                                <i class="fas fa-cloud-download-alt"></i>
                            </div>
                            <div class="btn-information-inside">
                                <h5 class="m-0 font-weight-bold font-description">Imagem 01</h5>
                                <h5 class="m-0 font-weight-bold font-description" id="photo_name">Indisponível</h5>
                            </div>
                        </a>
                    </div>
                </div>

                <div style="text-align-last: center;">
                    <button type="submit" class="btn btn-primary">Cadastrar Automóvel</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="editaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="padding:20px">
            <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h1>Editar Automóvel</h1>
            <form action="php/update/vehicles/edit.php" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="vehicle_id_edit" id="vehicle_id_edit">
                <input type="hidden" name="license_plate_old_edit" id="license_plate_old_edit">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label>Modelo</label>
                        <input id="model_edit" name="model_edit" type="text" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label>Marca</label>
                        <input id="brand_edit" name="brand_edit" type="text" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label>Cor</label>
                        <input id="color_edit" name="color_edit" type="text" class="form-control">
                    </div>
                    <div class="form-group col-md-2">
                        <label>Ano</label>
                        <input id="car_year_edit" name="car_year_edit" type="text" class="form-control">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label>Ano Modelo</label>
                        <input id="model_year_edit" name="model_year_edit" type="text" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label>Placa</label>
                        <input onkeydown="upperCase(this)" onchange="validaPlaca(this)" id="license_plate_edit" name="license_plate_edit" type="text" class="form-control" maxlength="7">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="owner_edit">Proprietário</label>
                        <select id="owner_edit" name="owner_edit" class="form-control">
                            <?php while ($row = mysqli_fetch_array($res_owner_edit)) { ?>
                                <option value="<?= $row['id'] ?>"><?= $row['fantasy_name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <input onchange="photo_insert(this)" type="file" id="vehicle_photo_edit" name="vehicle_photo_edit" style="display:none;" accept=".jpeg,.jpg,.png">
                        <a onclick="input_photo(this)" id="false_input_edit" class="btn btn-light btn-icon-split" style="position:relative; margin: 4px 0px;background: #2D2734;border: none;">
                            <div id="icon_1" class="icon-grey">
                                <i class="fas fa-cloud-download-alt"></i>
                            </div>
                            <div class="btn-information-inside">
                                <h5 class="m-0 font-weight-bold font-description">Imagem 01</h5>
                                <h5 class="m-0 font-weight-bold font-description" id="photo_name_edit">Indisponível</h5>
                            </div>
                        </a>
                    </div>
                </div>

                <div style="text-align-last: center;">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Editar Automóvel</button>
                </div>
            </form>
        </div>
    </div>
</div>



<!-- DESATIVAR -->
<div class="modal fade" id="modalOrcamento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" style="color: #01B93C;margin-left: 5px;" id="exampleModalLabel">Alerta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="php/update/onoff_cliente.php" method="POST">
                <input type="hidden" id="id_cliente_onoff" name="id_cliente_onoff">
                <div class="modal-body">
                    Ao checar os orçamentos deste veículo, <span style="color:#01B93C">você será
                        redirecionado para outra página.</span><br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Prosseguir</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modalDesativar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" style="color: #01B93C;margin-left: 5px;" id="exampleModalLabel">Alerta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="php/update/vehicles/onoff.php" method="POST">
                <input type="hidden" id="vehicle_id_onoff" name="vehicle_id_onoff">
                <input type="hidden" id="vehicle_status" name="vehicle_status">

                <div class="modal-body">
                    Tem certeza que deseja alterar o status deste automóvel <span style="color:#01B93C" id="vehicle_onoff"></span>?<br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Alterar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#DataTableClientes').DataTable();

    });

    function upperCase(obj) {
        setTimeout(function() {
            obj.value = obj.value.toUpperCase();
        }, 1);

    }

    function input_photo(obj) {
        if (obj.id.split("_").length > 2) {
            var id_complemento = obj.id.split("_")[2];
            // console.log(id_complemento);
            id_complemento = "_" + id_complemento;
        } else {
            var id_complemento = "";
        }
        $('#vehicle_photo' + id_complemento).click();
    }

    function photo_insert(obj) {
        if (obj.id.split("_").length > 2) {
            var id_complemento = obj.id.split("_")[2];
            // console.log(id_complemento);
            id_complemento = "_" + id_complemento;
        } else {
            var id_complemento = "";
        }
        $('#photo_name' + id_complemento).html($('#vehicle_photo' + id_complemento)[0].files[0].name);
        // $("#icon_2_insert").addClass('icon').removeClass('icon-grey');
    }

    function edit(id) {
        $.get("php/get/vehicles/index.php?id=" + id, function(data) {
            var json = JSON.parse(data)[0];
            $('#vehicle_id_edit').val(id);
            $('#owner_edit').val(json.client_id);
            $('#model_edit').val(json.model);
            $('#brand_edit').val(json.brand);
            $('#color_edit').val(json.color);
            $('#car_year_edit').val(json.car_year);
            $('#model_year_edit').val(json.model_year);
            $('#license_plate_old_edit').val(json.license_plate);
            $('#license_plate_edit').val(json.license_plate);
            $('#photo_name_edit').html(json.photo_name);

            // console.log($('#editaModal'));
            $('#editaModal').modal('show');
        });
    }

    function on_off(id, status) {
        $('#vehicle_id_onoff').val(id);
        $('#vehicle_status').val(status);

        var nome_stt = status == 0 ? "para inativo" : "para ativo";
        $('#status_nome').html(nome_stt);

        $.get("php/get/vehicles/index.php?id=" + id, function(data) {
            var json = JSON.parse(data)[0];

            $('#vehicle_onoff').html(json.model + "/" + json.license_plate);

            $('#modalDesativar').modal("show");
        });
    }


    function validaPlaca(obj) {
        var license_plate = obj.value;
        const regexPlaca_2 = /[A-Z]{3}[0-9][0-9A-Z][0-9]{2}/;
        const table = "vehicles";

        var complemento = obj.id.split("_");
        if (complemento.length > 2) {
            complemento = "_" + complemento[2];
            var license_plate_old = $('#license_plate_old_edit').val();
        } else {
            complemento = "";
        }


        if (license_plate != "") {
            if (!regexPlaca_2.test(license_plate)) {
                alert("Placa inválida");
                $('#license_plate' + complemento).val("");
            }
            $.get("php/get/utils/verify_license_plate.php?license_plate=" + license_plate + "&table=" + table, function(data) {
                if (parseInt(data) > 0) {
                    if (license_plate_old) {
                        // edit
                        if (license_plate != license_plate_old) {
                            $('#license_plate' + complemento).val("");
                            alert("Placa já cadastrada!");
                        }
                    } else {
                        // cadastro
                        $('#license_plate').val("");
                        alert("Placa já cadastrada!");
                    }
                }
            });
        }
    }
</script>