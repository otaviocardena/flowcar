<?php
include_once("../php/conn/index.php");

$sql = "SELECT * FROM employees";
$res_employees = mysqli_query($conn, $sql);

$sql = "SELECT * FROM permissions";
$res_permissions = mysqli_query($conn, $sql);
$res_permissions_edit = mysqli_query($conn, $sql);
?>
<div class="container-fluid">
    <div class="card shadow mb-4" style="height: 100%;">
        <div class="card-header py-3" style="position: relative; display:flex">
            <h6 class="m-0 font-weight-bold text-primary">Consultar funcionario</h6>
            <div class="nav-search-btn">
                <button class="btn btn-primary" style="width: 100%;margin-right: 10px;border-radius: 25px;" data-toggle="modal" data-target="#cadastrarModal">
                    <i class="fas fa-plus"></i>
                    <span>Cadastrar funcionario</span>
                </button>
            </div>
        </div>
        <div class="card-body">
            <table class="table" id="DataTablefuncionarios">
                <thead>
                    <tr>
                        <th scope="col">Nome</th>
                        <th scope="col">CPF</th>
                        <th scope="col">Cargo</th>
                        <th scope="col">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php while ($row = mysqli_fetch_array($res_employees)) { ?>
                        <tr>
                            <td><?= $row['name'] ?></td>
                            <td><?= $row['document'] ?></td>
                            <td><?= $row['role'] ?></td>
                            <td>
                                <button type="button" onclick="edit(<?= $row['id'] ?>)" class="btn btn-primary" style="border-radius: 25px;padding: 6px 9px;">
                                    <i class="far fa-eye"></i>
                                </button>
                                <?php if ($row['status'] == 1) { ?>
                                    <button type="button" onclick="on_off(<?= $row['id'] ?>,0)" class="btn btn-danger" style="border-radius: 25px;padding: 6px 12px;">
                                        <i class="fas fa-power-off"></i>
                                    </button>
                                <?php } else if ($row['status'] == 0) { ?>
                                    <button type="button" onclick="on_off(<?= $row['id'] ?>,1)" class="btn btn-primary" style="border-radius: 25px;padding: 6px 12px;">
                                        <i class="fas fa-power-off"></i>
                                    </button>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>



<div class="modal fade" id="cadastrarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="padding:20px">
            <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h1>Cadastrar funcionario</h1>
            <form action="php/insert/employees/index.php" method="POST" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="name">Nome</label>
                        <input type="text" id="name" name="name" class="form-control" placeholder="Nome" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="document">CPF</label>
                        <input onchange="verificar_documento(this)" type="text" id="document" name="document" class="form-control" placeholder="999.999.999-99" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="salary">Salário</label>
                        <input type="number" step="0.01" id="salary" name="salary" class="form-control" placeholder="999,99" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="role">Cargo</label>
                        <input type="text" id="role" name="role" class="form-control" placeholder="Cargo" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="login">Login</label>
                        <input onchange="verificar_login(this)" type="text" id="login" name="login" class="form-control" placeholder="Login" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="password">Senha</label>
                        <input type="password" id="password" name="password" class="form-control" placeholder="Senha" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="zip">CEP</label>
                        <input onchange="busca_cep(this)" type="text" id="zip" name="zip" class="form-control" placeholder="99999-999" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="stret">Endereço</label>
                        <input type="text" id="street" name="street" class="form-control" placeholder="Endereço" required>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="number">Número</label>
                        <input type="number" step="1" id="number" name="number" class="form-control" placeholder="999" required>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="neighborhood">Bairro</label>
                        <input type="text" id="neighborhood" name="neighborhood" class="form-control" placeholder="Bairro" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="city">Cidade</label>
                        <input type="text" id="city" name="city" class="form-control" placeholder="Cidade" required>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="state">UF</label>
                        <select id="state" name="state" class="form-control" placehodlder="UF" required>
                            <option value="AC">AC</option>
                            <option value="AL">AL</option>
                            <option value="AP">AP</option>
                            <option value="AM">AM</option>
                            <option value="BA">BA</option>
                            <option value="CE">CE</option>
                            <option value="DF">DF</option>
                            <option value="ES">ES</option>
                            <option value="GO">GO</option>
                            <option value="MA">MA</option>
                            <option value="MT">MT</option>
                            <option value="MS">MS</option>
                            <option value="MG">MG</option>
                            <option value="PA">PA</option>
                            <option value="PB">PB</option>
                            <option value="PR">PR</option>
                            <option value="PE">PE</option>
                            <option value="PI">PI</option>
                            <option value="RJ">RJ</option>
                            <option value="RN">RN</option>
                            <option value="RS">RS</option>
                            <option value="RO">RO</option>
                            <option value="RR">RR</option>
                            <option value="SC">SC</option>
                            <option value="SP">SP</option>
                            <option value="SE">SE</option>
                            <option value="TO">TO</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="complement">Complemento</label>
                        <input type="text" id="complement" name="complement" class="form-control" placeholder="Complemento">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="permissions">Permissões</label>
                        <select placeholder="Clique e insira as permissões" id="permissions" name="permissions[]" multiple="multiple" required>
                            <?php while ($row = mysqli_fetch_array($res_permissions)) { ?>
                                <option value="<?= $row['id'] ?>"><?= $row['permission'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div style="text-align-last: center;">
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="editaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="padding:20px">
            <button style="width: fit-content;place-self: flex-end;" class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h1>Editar funcionario</h1>
            <form action="php/update/employees/edit.php" method="POST">
                <input type="hidden" name="employee_id_edit" id="employee_id_edit">
                <input type="hidden" name="login_old_edit" id="login_old_edit">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="name_edit">Nome</label>
                        <input type="text" id="name_edit" name="name_edit" class="form-control" placeholder="Nome" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="document_edit">CPF</label>
                        <input onchange="verificar_documento(this)" type="text" id="document_edit" name="document_edit" class="form-control" placeholder="999.999.999-99" required>
                        <input type="hidden" id="document_edit_old" name="document_edit_old">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="salary_edit">Salário</label>
                        <input type="number" step="0.01" id="salary_edit" name="salary_edit" class="form-control" placeholder="999,99" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="role_edit">Cargo</label>
                        <input type="text" id="role_edit" name="role_edit" class="form-control" placeholder="Cargo" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="login_edit">Login</label>
                        <input onchange="verificar_login(this)" type="text" id="login_edit" name="login_edit" class="form-control" placeholder="Login" required>
                        <input type="hidden" id="login_edit_old" name="login_edit_old">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="password_edit">Senha</label>
                        <input type="password" id="password_edit" name="password_edit" class="form-control" placeholder="Senha" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="zip_edit">CEP</label>
                        <input onchange="busca_cep(this)" type="text" id="zip_edit" name="zip_edit" class="form-control" placeholder="99999-999" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="stret_edit">Endereço</label>
                        <input type="text" id="street_edit" name="street_edit" class="form-control" placeholder="Endereço" required>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="number_edit">Número</label>
                        <input type="number" step="1" id="number_edit" name="number_edit" class="form-control" placeholder="999" required>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="neighborhood_edit">Bairro</label>
                        <input type="text" id="neighborhood_edit" name="neighborhood_edit" class="form-control" placeholder="Bairro" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="city_edit">Cidade</label>
                        <input type="text" id="city_edit" name="city_edit" class="form-control" placeholder="Cidade" required>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="state_edit">UF</label>
                        <select id="state_edit" name="state_edit" class="form-control" placehodlder="UF" required>
                            <option value="AC">AC</option>
                            <option value="AL">AL</option>
                            <option value="AP">AP</option>
                            <option value="AM">AM</option>
                            <option value="BA">BA</option>
                            <option value="CE">CE</option>
                            <option value="DF">DF</option>
                            <option value="ES">ES</option>
                            <option value="GO">GO</option>
                            <option value="MA">MA</option>
                            <option value="MT">MT</option>
                            <option value="MS">MS</option>
                            <option value="MG">MG</option>
                            <option value="PA">PA</option>
                            <option value="PB">PB</option>
                            <option value="PR">PR</option>
                            <option value="PE">PE</option>
                            <option value="PI">PI</option>
                            <option value="RJ">RJ</option>
                            <option value="RN">RN</option>
                            <option value="RS">RS</option>
                            <option value="RO">RO</option>
                            <option value="RR">RR</option>
                            <option value="SC">SC</option>
                            <option value="SP">SP</option>
                            <option value="SE">SE</option>
                            <option value="TO">TO</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="complement_edit">Complemento</label>
                        <input type="text" id="complement_edit" name="complement_edit" class="form-control" placeholder="Complemento">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="permissions_edit">Permissões</label>
                        <select placeholder="Clique e insira as permissões" id="permissions_edit" name="permissions_edit[]" multiple="multiple" required>
                            <?php while ($row = mysqli_fetch_array($res_permissions_edit)) { ?>
                                <option value="<?= $row['id'] ?>"><?= $row['permission'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div style="text-align-last: center;">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Editar</button>
                </div>
            </form>
        </div>
    </div>
</div>



<!-- DESATIVAR -->
<div class="modal fade" id="modalDesativar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" style="color: #01B93C;margin-left: 5px;" id="exampleModalLabel">Alerta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="php/update/employees/onoff.php" method="POST">
                <input type="hidden" id="employee_id" name="employee_id">
                <input type="hidden" id="employee_status" name="employee_status">
                <div class="modal-body">
                    Tem certeza que deseja alterar o status do funcionário <span style="color:#01B93C" id="funcionario_onoff"></span><span id="status_nome"></span>?<br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Alterar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#DataTablefuncionarios').DataTable();

        $('#document').mask('999.999.999-99');
        $('#document_edit').mask('999.999.999-99');
        $('#zip').mask('99999-999');
        $('#zip_edit').mask('99999-999');
    });

    $(document).ready(function() {
        $('#permissions').selectize({
            plugins: ["remove_button"]
        });
    });
    $(document).ready(function() {
        $('#permissions_edit').selectize({
            plugins: ["remove_button"]
        });
    });

    function busca_cep(obj) {
        var complemento = obj.id.split("_");
        if (complemento.length > 1) {
            complemento = "_" + complemento[1];
        } else {
            complemento = "";
        }
        console.log(complemento);

        //Nova variável "cep" somente com dígitos.
        var cep = obj.value.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if (validacep.test(cep)) {

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $('#street' + complemento).val(dados.logradouro);
                        $('#neighborhood' + complemento).val(dados.bairro);
                        $('#city' + complemento).val(dados.localidade);
                        $('#state' + complemento).val(dados.uf);
                        // $('#complemento').val(dados.complemento);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                alert("Formato de CEP inválido.");
            }
        }
    }

    function verificar_documento(obj) {
        var documento = obj.value;
        var tabela = "employees";

        var complemento = obj.id.split("_");
        if (complemento.length > 1) {
            complemento = "_" + complemento[1];
            var documento_old = $('#document_edit_old').val();
        } else {
            complemento = "";
        }


        $.get("php/get/utils/verify_document.php?doc=" + documento + "&table=" + tabela, function(data) {
            if (parseInt(data) > 0) {
                if (documento_old) {
                    // edit
                    if (documento != documento_old) {
                        if (obj.value != "") {
                            $('#document' + complemento).val("");
                            alert("Documento já cadastrado!");
                        }
                    }
                } else {
                    // cadastro
                    if (obj.value != "") {
                        $('#document' + complemento).val("");
                        alert("Documento já cadastrado!");
                    }
                }
            }
        });
    }

    function verificar_login(obj) {
        var login = obj.value;
        var table = "employees";

        var complemento = obj.id.split("_");
        if (complemento.length >= 2) {
            complemento = "_" + complemento[1];
            var login_old = $('#login_old_edit').val();
        } else {
            complemento = "";
        }


        $.get("php/get/utils/verify_login.php?login=" + login + "&table=" + table, function(data) {
            if (parseInt(data) > 0) {
                if (login_old) {
                    // edit
                    if (login != login_old) {
                        $('#login' + complemento).val("");
                        alert("Login já existente!");
                    }
                } else {
                    // cadastro
                    $('#login').val("");
                    alert("Login já existente!");
                }
            }
        });
    }

    function edit(id) {
        $.get("php/get/employees?id=" + id, function(data) {
            var json = JSON.parse(data)[0];
            $('#employee_id_edit').val(id);
            $('#login_old_edit').val(json.login);
            $('#login_edit').val(json.login);
            $('#password_edit').val(json.password);
            $('#name_edit').val(json.name);
            $('#document_edit_old').val(json.document);
            $('#document_edit').val(json.document);
            $('#salary_edit').val(json.salary_amount);
            $('#role_edit').val(json.role);
            $('#zip_edit').val(json.zip);
            $('#street_edit').val(json.street);
            $('#number_edit').val(json.number);
            $('#neighborhood_edit').val(json.neighborhood);
            $('#city_edit').val(json.city);
            $('#state_edit').val(json.state);
            $('#complement_edit').val(json.complement);

            var permissions = json.permissions;
            permissions = permissions.split(',');

            var selectize_permissions = document.getElementById("permissions_edit");
            selectize_permissions.selectize.setValue(permissions);

            // console.log($('#editaModal'));
            $('#editaModal').modal('show');
        });
    }

    function on_off(id, status) {
        $('#employee_id').val(id);
        $('#employee_status').val(status);

        var nome_stt = status == 0 ? "para inativo" : "para ativo";
        $('#status_nome').html(nome_stt);

        $.get("php/get/employees/index.php?id=" + id, function(data) {
            var json = JSON.parse(data)[0];

            $('#funcionario_onoff').html(json.name);

            $('#modalDesativar').modal("show");
        });
    }
</script>