<?php
session_start();
include_once("php/conn/index.php");

if (isset($_SESSION['ZWxldHJpY2Ft'])) {
  $login_id = $_SESSION['ZWxldHJpY2Ft'];

  $sql = "SELECT * FROM employees WHERE id = $login_id";
  $res = mysqli_query($conn, $sql);
  while ($row = mysqli_fetch_array($res)) {
    $nome_logado = $row['name'];
  }
} else {
  header('Location: login.php');
}

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <link href="img/favicon.png" rel="icon">
  <link href="img/favicon.png" rel="apple-touch-icon">

  <title>Flow Car</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.1/css/selectize.default.css" rel="stylesheet" type="text/css">

  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">

  <!-- Custom styles for this template-->
  <link href="css/style.css" rel="stylesheet">

  <link href="css/calendar.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav sidebar sidebar-dark accordion" style="background: #413B49;" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <div class="sidebar-brand-icon">
          <img src="./img/logo-lateral.png" alt="" style="width:80%">
        </div>
        <!-- <div class="sidebar-brand-text mx-3" style="color:#fff">FlowCar</div> -->
      </a>

      <!-- Divider -->
      <?php if (in_array("1", $_SESSION['permissions']) || in_array("2", $_SESSION['permissions'])) { ?>
        <hr class="sidebar-divider my-0">
      <?php } ?>

      <!-- Nav Item - Dashboard -->
      <?php if (in_array("1", $_SESSION['permissions'])) { ?>
        <li class="nav-item active">
          <a class="nav-link" onclick="page('agenda.php')">
            <i class="fas fa-fw fa-home"></i>
            <span>Agenda</span></a>
        </li>
      <?php } ?>

      <?php if (in_array("2", $_SESSION['permissions'])) { ?>
        <li class="nav-item">
          <a class="nav-link" onclick="page('orcamento.php')">
            <i class="fas fa-fw fa-home"></i>
            <span>Orçamento</span></a>
        </li>
      <?php } ?>

      <?php if (in_array("3", $_SESSION['permissions']) || in_array("4", $_SESSION['permissions']) || in_array("5", $_SESSION['permissions']) || in_array("6", $_SESSION['permissions']) || in_array("7", $_SESSION['permissions']) || in_array("8", $_SESSION['permissions'])) { ?>
        <hr class="sidebar-divider my-0">
      <?php } ?>

      <?php if (in_array("3", $_SESSION['permissions'])) { ?>
        <li class="nav-item">
          <a class="nav-link" onclick="page('cliente.php')">
            <i class="fas fa-fw fa-briefcase"></i>
            <span>Cliente</span></a>
          </a>
        </li>
      <?php } ?>

      <?php if (in_array("4", $_SESSION['permissions'])) { ?>
        <li class="nav-item">
          <a class="nav-link" onclick="page('fornecedor.php')">
            <i class="fas fa-fw fa-user"></i>
            <span>Fornecedor</span></a>
        </li>
      <?php } ?>

      <?php if (in_array("5", $_SESSION['permissions'])) { ?>
        <li class="nav-item">
          <a class="nav-link" onclick="page('funcionario.php')">
            <i class="fas fa-fw fa-file-alt"></i>
            <span>Funcionário</span></a>
        </li>
      <?php } ?>

      <?php if (in_array("6", $_SESSION['permissions'])) { ?>
        <li class="nav-item">
          <a class="nav-link" onclick="page('automoveis.php')">
            <i class="fas fa-car"></i>
            <span>Automóveis</span></a>
        </li>
      <?php } ?>

      <?php if (in_array("7", $_SESSION['permissions'])) { ?>
        <li class="nav-item">
          <a class="nav-link" onclick="page('produtos.php')">
            <i class="fas fa-prescription-bottle"></i>
            <span>Produtos</span></a>
        </li>
      <?php } ?>

      <?php if (in_array("8", $_SESSION['permissions'])) { ?>
        <li class="nav-item">
          <a class="nav-link" onclick="page('equipamentos.php')">
            <i class="fas fa-truck"></i>
            <span>Equipamentos</span></a>
        </li>
      <?php } ?>



      <?php if (in_array("9", $_SESSION['permissions']) || in_array("10", $_SESSION['permissions']) || in_array("11", $_SESSION['permissions'])) { ?>
        <hr class="sidebar-divider d-none d-md-block">
      <?php } ?>

      <?php if (in_array("9", $_SESSION['permissions'])) { ?>
        <!-- Divider -->
        <li class="nav-item">
          <a class="nav-link" onclick="page('servico.php')">
            <i class="fas fa-cog"></i>
            <span>Serviço</span></a>
        </li>
      <?php } ?>

      <?php if (in_array("10", $_SESSION['permissions'])) { ?>
        <li class="nav-item">
          <a class="nav-link" onclick="page('contas-pagar.php')">
            <i class="fas fa-less-than"></i>
            <span>Contas à pagar</span></a>
        </li>
      <?php } ?>

      <?php if (in_array("11", $_SESSION['permissions'])) { ?>
        <li class="nav-item">
          <a class="nav-link" onclick="page('contas-receber.php')">
            <i class="fas fa-greater-than"></i>
            <span>Contas à Receber</span></a>
        </li>
      <?php } ?>


      <hr class="sidebar-divider d-none d-md-block">
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseData" aria-expanded="true" aria-controls="collapseData">
          <i class="fas fa-fw fa-briefcase"></i>
          <span>Meu perfil</span></a>
        </a>
        <div id="collapseData" class="collapse" aria-labelledby="headingData" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" onclick="page('dados-empresa.php')">Dados da empresa</a>
            <a class="collapse-item" onclick="page('profile.php')">Meus dados</a>
          </div>
        </div>
      </li>
      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content" style="background: #1F1B24;">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow" style="background: #2D2734 !important;">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>


          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Alerts -->


            <div class="topbar-divider d-none d-sm-block"></div>
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-white-600 small"><?= $nome_logado ?></span>
                <img class="img-profile rounded-circle" src="./img/profile.png">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" onclick="page('profile.php')" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Perfil
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" style="cursor:pointer;" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Sair
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div id="conteudo"></div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; EvolutionSoft 2021</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- LOGOUT MODAL -->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title font-weight-bold" style="color: #01B93C;margin-left: 5px;" id="exampleModalLabel">Alerta</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form>
          <input type="hidden" id="employee_id" name="employee_id">
          <input type="hidden" id="employee_status" name="employee_status">
          <div class="modal-body">
            Tem certeza que deseja <span style="color:#01B93C">sair</span>?<br>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            <a href="php/auth/logout.php" class="btn btn-primary">Sair</a>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.1/js/standalone/selectize.min.js"></script>

  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

  <script src="js/sb-admin-2.min.js"></script>

  <script type="text/javascript" charset="utf8" src="js/dataTables.js"></script>



  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js" integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ==" crossorigin="anonymous"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.js" integrity="sha512-otOZr2EcknK9a5aa3BbMR9XOjYKtxxscwyRHN6zmdXuRfJ5uApkHB7cz1laWk2g8RKLzV9qv/fl3RPwfCuoxHQ==" crossorigin="anonymous"></script>

  <script type="text/javascript" src="js/jquery-ui-datepicker.min.js"></script>
  <script>
    if (window.location.hash) {
      var hash = window.location.hash.substring(1);
      window.location.hash = window.location.hash.split(hash)[0];
      $('#conteudo').load("views/" + hash);
    } else {
      $('#conteudo').load("views/<?= $_SESSION['tela_inicial'] ?>.php");
    }

    function page(pagina) {
      var data = "<div id='spinner' class='spinner-border' role='status' style='margin-left: 47%;margin-top: 20%;margin-bottom: 20%; color:#21613A; width:5rem; height:5rem;'><span class='sr-only'>Loading...</span></div>";
      $("#conteudo").html(data);
      $(document).ready(function() {
        $('#conteudo').load("views/" + pagina);
      });
    }
  </script>

</body>

</html>